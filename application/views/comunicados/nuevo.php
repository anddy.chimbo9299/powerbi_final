<div class="container-fluid">
    <div class="card">
        <div class="card-body">
            <center>
                <h1><b>FORMULARIO INGRESO NUEVO COMUNICADO</b></h1>
            </center>
            <form action="<?php echo site_url('/Comunicados/GuardarComunicado') ?>" method="post" id="frm_nuevo_comunicado">
                <div class="row g-3 justify-content-center">
                    <div class="col-md-4">
                        <div class="mb-3">
                            <input type="hidden" class="form-control" name="id_com" id="id_com" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                </div>
                <div class="row g-3 justify-content-center">
                    <div class="col-md-4">
                        <div class="mb-3">
                            <label for="fecha_com" class="form-label">FECHA:</label>
                            <input type="date" class="form-control" name="fecha_com" id="fecha_com" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="mb-3">
                            <label for="mensaje_com" class="form-label">MENSAJE:</label>
                            <input type="text" class="form-control" name="mensaje_com" id="mensaje_com" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                </div>
                <div class="row g-3 justify-content-center">
                    <div class="col-md-4">
                        <div class="mb-3">
                            <label for="actualizacion_com" class="form-label">ACTUALIZACION:</label>
                            <input type="datetime-local" class="form-control" name="actualizacion_com" id="actualizacion_com" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="mb-3">
                            <label for="creacion_com" class="form-label">CREACION:</label>
                            <input type="datetime-local" class="form-control" name="creacion_com" id="creacion_com" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                </div>
                <div class="row g-3 justify-content-center">
                    <div class="col-md-12 text-center">
                        <button class="btn btn-primary" type="submit">Guardar</button>
                        <a name="" id="" class="btn btn-dark" href="<?php echo site_url('/Comunicados/index') ?>" role="button">Cancelar</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    $("#frm_nuevo_comunicado").validate({
        rules: {
            nombre_imp: {
                required: true,
                minlength: 10,
                maxlength: 10,
                digits: true
            },
            descripcion_imp: {
                required: true,

                letras: true
            },
            porcentaje_imp: {
                required: true,

                letras: true
            },
            


        },
        messages: {
            nombre_imp: {
                required: "Nesesariamente debe elegir una",
                minlength: "Cedula incorrecta",
                maxlength: "Cedula incorrecta 10 digitos",
                digits: "Este campo solo acepta numeros",
                number: "Este campo solo acepta numeros",
            },
            descripcion_imp: {
                required: "Porfavor ingrese la descripcion del delito",
                // minlength: "El apellido debe tener minimo 3 digitos ",
                // maxlength: "APellido incorrecto"
            },
            porcentaje_imp: {
                required: "debe ingresar el tiempo de condena para el delito",
                // minlength: "El nombre debe tener 3 dígitos",
                // maxlength: "Nombres incorrectos 50 dígitos",
            },
            


        }
    });
</script>

</div>
