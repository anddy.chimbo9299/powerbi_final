<div  class= "contenedor1">
<div class="row">
    <div class="col-md-8">
    <center><h1> Listado de Asistencia</h1></center>
    </div>
    <div class="col-md-4">
        <a href="<?php echo site_url('asistencias/nuevo');?>" class="btn btn-primary"> Nuevo
        <i class="glyphicon glyphicon-plus"></i></a>
    </div>
</div>
<br>
<?php if ($asistencias):?>
<table class="table table-striped table-bordered table-hover" id="tablaAsistencias">
    <thead>
        <tr>
            <th>ID</th>
            <th>Tipo</th>
            <th>Valor</th>
            <th>Atraso</th>
            <th>Valor de Atraso</th>
            <th>Creacion</th>
            <th>Actulizacion</th>
            <th>Acciones</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($asistencias as $filaTemporal):?>
            <tr>
                <td>
                    <?php echo $filaTemporal->id_asi;?>
                </td>
                <td>
                  <?php echo $filaTemporal->tipo_asi;?> </td>
                <td>
                  <?php echo $filaTemporal->valor_asi;?></td>
                <td>
                  <?php echo $filaTemporal->atraso_asi;?></td>
                <td>
                  <?php echo $filaTemporal->valor_atraso_asi;?></td>
                  <td>
                    <?php echo $filaTemporal->creacion_asi;?></td>
                    <td>
                      <?php echo $filaTemporal->actualizacion_asi;?></td>

                    <td class="text-center">
                        <a href="<?php echo site_url(); ?>/asistencias/editar/<?php echo $filaTemporal->id_asi;?>" title="Editar"  >
                            <i class="mdi mdi-pencil"></i>
                            Editar
                        </a>
                        &nbsp;&nbsp;&nbsp;
                        <a href="<?php echo site_url(); ?>/asistencias/eliminar/<?php echo $filaTemporal->id_asi;?>" title="Eliminar" style="color:red;"  onclick="return confirm('Estas seguro de borrar este registro?');">
                            <i class="mdi mdi-close"></i>
                            Eliminar
                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <?php else: ?>
        <h1>No existe</h1>
        <?php endif; ?>
    </div>
<script type="text/javascript">
    $("#tablaAsistencias")
    .DataTable();
</script>
