
<div class="row">
    <div class="col-md-12">
    <center>
        <br>
           <div class="row">
           <h1>Listado de Historiales</h1>
            <div class="col-4">
                <br>
                
                 <a name="" id="" class="btn btn-primary" href="<?php echo site_url('/historiales/nuevo') ?>" role="button">Agregar Nuevo </a>
                </div>
            
           </div>
        </center>
    </div>
</div>
<br>
<br>
<div class="row">
    <div class="col-md-12">
        <?php if ($historial): ?>
            <table class="table  table-striped" id="tablaLectura">
                <thead>
                    <th>ID</th>
                    <th>Medidor</th>
                    <th>Nombre Socio</th>
                    <th>Fecha Actualizacion</th>
                    <th>Estado</th>
                    <th>Observacion</th>
                    <th>Fecha Cambio</th>
                    <th>Creacion</th>
                    <th>Propietario Actual</th>
                    <th>Acciones</th>
                </thead>
                
                <tbody>
                    <?php foreach ($historial as $filaTemporal): ?>
                        <tr>
                            <td><?php echo $filaTemporal->id_his ?></td>
                            <td><?php echo $filaTemporal->fk_id_med ?></td>
                            <td><?php echo $filaTemporal->fk_id_soc ?> </td>
                            <td> <?php echo $filaTemporal->actualizacion_his ?></td>
                            <td><?php echo $filaTemporal->estado_his ?></td>
                            <th><?php echo $filaTemporal->observacion_his ?></th>
                            <th><?php echo $filaTemporal->fecha_cambio_his ?></th>
                            <th><?php echo $filaTemporal->creacion_his ?></th>
                            <th><?php echo $filaTemporal->propietario_actual_his ?></th>
                            
                            <td class="text-center" >
                                <a href="<?php echo site_url(); ?>/historiales/editar/<?php echo $filaTemporal->id_his; ?>" title="Editar Historial" >
                                    <i class="mdi  mdi-pencil">Editar</i>
                                </a>
                                &nbsp;&nbsp;

                                <a href="<?php echo site_url(); ?>/historiales/eliminar/<?php echo $filaTemporal->id_his; ?>" title="Borrar Historial" style="color:red" onclick="return confirm('Esta seguro de borra el registro?');">
                                    <i class="mdi  mdi-close">Eliminar</i>
                                </a>
            
                            </td>
                        </tr>         

                    <?php endforeach; ?>
                    </tbody>
                </table>

                <?php else: ?>
                <h1>No hay datos</h1>

        <?php endif; ?>

       
    </div>
</div>

<script type="text/javascript">
    $("#tablaLectura")
    .DataTable();
</script>