<div class="container-fluid">
    <div class="row justify-content-center align-items-center g-2">
        <div class="card">
        <center>
        <br>
           <div class="row">
            <h3 class="col-8">
                <b>TABLA DE COMUNICADOS</b>
            </h3>
            <div class="col-4">
                <br>
                
                 <a name="" id="" class="btn btn-primary" href="<?php echo site_url('/Comunicados/nuevo') ?>" role="button">NUEVO COMUNICADO</a>
                </div>
            
           </div>
        </center>
            <div class="card-body">
            <div class="table-responsive">
                <?php if($comunicadosIndex){ ?>
                <table class="table table-light" id="tbl_comunicados">
                    <thead class="table table-dark">
                        <tr>
                            <th scope="col">ID:</th>
                            <th scope="col">FECHA:</th>
                            <th scope="col">MENSAJE</th>
                            <th scope="col">ACTUALIZACION </th>
                            <th scope="col">CREACION </th>
                            <th scope="col">ACCIONES </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($comunicadosIndex as $registro){ ?>
                        <tr>
                            <td><?php echo $registro->id_com ?></td>
                            <td><?php echo $registro->fecha_com ?></td>
                            <td><?php echo $registro->mensaje_com?></td>
                            <td><?php echo $registro->actualizacion_com ?></td>
                            <td><?php echo $registro->creacion_com ?></td>
                           
                            <td class="text-center">
                            <a href="<?php echo site_url("Comunicados/editar/$registro->id_com") ?>" title="Editar Comunicados" ><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil" viewBox="0 0 16 16">
                                    <path d="M12.146.146a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1 0 .708l-10 10a.5.5 0 0 1-.168.11l-5 2a.5.5 0 0 1-.65-.65l2-5a.5.5 0 0 1 .11-.168l10-10zM11.207 2.5 13.5 4.793 14.793 3.5 12.5 1.207 11.207 2.5zm1.586 3L10.5 3.207 4 9.707V10h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.293l6.5-6.5zm-9.761 5.175-.106.106-1.528 3.821 3.821-1.528.106-.106A.5.5 0 0 1 5 12.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.468-.325z" />
                                </svg></a> &nbsp;&nbsp;&nbsp;
                               

                                <a href="<?php echo site_url("Comunicados/Eliminar/$registro->id_com") ?>" title="Eiminar comunicado" onclick="return confirm('Estas seguro');" style="color: red;">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16" style="color: red;">
                                    <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5Zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5Zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6Z" />
                                    <path d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1ZM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118ZM2.5 3h11V2h-11v1Z" />
                                </svg>
                            </a>
                            

                        </td>
                    </tr>
                    <?php } ?>
                        
                   
                    </tbody>
                </table>
                <?php  }else{
                    echo "No hay datos ";
                } ?>
            </div>
            
        </div>
        </div>
        </div>
        </div>
    
    </div>
</div>
<script type="text/javascript">
    $("#tbl_comunicados")
        .DataTable();
</script>