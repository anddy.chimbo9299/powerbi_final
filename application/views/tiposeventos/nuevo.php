<div class="container-fluid">
    <div class="card">
        <center>
            <h1>
                <b>
                    FORMULARIO INGRESO NUEVO TIPO DE EVENTO
                </b>
            </h1>
        </center>
        <div class="card-body">
            <form action="<?php echo site_url('/Tiposeventos/GuardarTipoevento') ?>" method="post" id="frm_nuevo_tipoevento">
                <div class="row justify-content-center align-items-center g-2">

                    <div class="col-md-4">
                        <div class="mb-3">
                            <label for="nombre_te" class="form-label">NOMBRE:</label>
                            <input  type="text" class="form-control" name="nombre_te" id="nombre_te" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="mb-3">
                            <label for="estado_te" class="form-label">ESTADO:</label>
                            <input  type="text" class="form-control" name="estado_te" id="estado_te" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="mb-3">
                            <label for="creacion_te" class="form-label">CREACION:</label>
                            <input  type="datetime-local" class="form-control" name="creacion_te" id="creacion_te" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="mb-3">
                            <label for="actualizacion_te" class="form-label">ACTUALIZACION:</label>
                            <input type="datetime-local" class="form-control" name="actualizacion_te" id="actualizacion_te" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                </div>
                <!-- Nueva fila para los botones -->
                <div class="row justify-content-center align-items-center g-2">
                    <div class="col-md-12 text-center">
                        <button class="btn btn-primary" type="submit">Guardar</button>
                        <a name="" id="" class="btn btn-dark" href="<?php echo site_url('/Tiposeventos/index') ?>" role="button">Cancelar</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    $("#frm_nuevo_teuesto").validate({
        rules: {
            nombre_te: {
                required: true,
                minlength: 10,
                maxlength: 10,
                digits: true
            },
            descripcion_te: {
                required: true,

                letras: true
            },
            porcentaje_te: {
                required: true,

                letras: true
            },
            


        },
        messages: {
            nombre_te: {
                required: "Nesesariamente debe elegir una",
                minlength: "Cedula incorrecta",
                maxlength: "Cedula incorrecta 10 digitos",
                digits: "Este campo solo acepta numeros",
                number: "Este campo solo acepta numeros",
            },
            descripcion_te: {
                required: "Porfavor ingrese la descripcion del delito",
                // minlength: "El apellido debe tener minimo 3 digitos ",
                // maxlength: "APellido incorrecto"
            },
            porcentaje_te: {
                required: "debe ingresar el tiempo de condena para el delito",
                // minlength: "El nombre debe tener 3 dígitos",
                // maxlength: "Nombres incorrectos 50 dígitos",
            },
            


        }
    });
</script>

</div>
