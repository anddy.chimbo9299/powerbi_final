<div class="container-fluid">
    <div class="card">
        <center>
            <h1>
                <b>
                    FORMULARIO INGRESO NUEVA TARIFA
                </b>
            </h1>
        </center>
        <div class="card-body">
            <form action="<?php echo site_url('/Tarifas/GuardarTarifa') ?>" method="post" id="frm_nuevo_tarifa">
                <div class="row justify-content-center align-items-center g-2">

                    <div class="col-md-4">
                    <div class="mb-3">
                            <input type="hidden"   type="text" class="form-control" name="id_tar" id="id_tar" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                <div class="row justify-content-center align-items-center g-2">
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="nombre_tar" class="form-label">NOMBRE:</label>
                            <input   type="text" class="form-control" name="nombre_tar" id="nombre_tar" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                    
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="descripcion_tar" class="form-label">DESCRIPCION:</label>
                            <input  type="text" class="form-control" name="descripcion_tar" id="descripcion_tar" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="estado_tar" class="form-label">ESTADO:</label>
                            <input type="text" class="form-control" name="estado_tar" id="estado_tar" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="m3_maximo_tar" class="form-label">TARIFA MAXIMA:</label>
                            <input  type="number" step="0.01" inputmode="decimal" class="form-control" name="m3_maximo_tar" id="m3_maximo_tar" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="tarifa_basica_tar" class="form-label">TARIFA BASICA:</label>
                            <input type="number" step="0.01" inputmode="decimal"  class="form-control" name="tarifa_basica_tar" id="tarifa_basica_tar" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="tarifa_excedente_tar" class="form-label">TARIFA EXCEDENTE:</label>
                            <input type="number" step="0.01" inputmode="decimal" class="form-control" name="tarifa_excedente_tar" id="tarifa_excedente_tar" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="valor_mora_tar" class="form-label">TARIFA MORA:</label>
                            <input type="number" step="0.01" inputmode="decimal" class="form-control" name="valor_mora_tar" id="valor_mora_tar" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                <!-- Nueva fila para los botones -->
                <div class="row justify-content-center align-items-center g-2">
                    <div class="col-md-12 text-center">
                        <button class="btn btn-primary" type="submit">Guardar</button>
                        <a name="" id="" class="btn btn-dark" href="<?php echo site_url('/Tarifas/index') ?>" role="button">Cancelar</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</div>
</div>

<script type="text/javascript">
    $("#frm_nuevo_tarifa").validate({
        rules: {
            nombre_imp: {
                required: true,
                minlength: 10,
                maxlength: 10,
                digits: true
            },
            descripcion_imp: {
                required: true,

                letras: true
            },
            porcentaje_imp: {
                required: true,

                letras: true
            },
            


        },
        messages: {
            nombre_imp: {
                required: "Nesesariamente debe elegir una",
                minlength: "Cedula incorrecta",
                maxlength: "Cedula incorrecta 10 digitos",
                digits: "Este campo solo acepta numeros",
                number: "Este campo solo acepta numeros",
            },
            descripcion_imp: {
                required: "Porfavor ingrese la descripcion del delito",
                // minlength: "El apellido debe tener minimo 3 digitos ",
                // maxlength: "APellido incorrecto"
            },
            porcentaje_imp: {
                required: "debe ingresar el tiempo de condena para el delito",
                // minlength: "El nombre debe tener 3 dígitos",
                // maxlength: "Nombres incorrectos 50 dígitos",
            },
            


        }
    });
</script>

</div>
