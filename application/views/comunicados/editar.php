<div class="container-fluid">
    <div class="card">
        <center>
            <h1>
                <b>
                    FORMULARIO EDITAR COMUNICADO
                </b>
            </h1>
        </center>
        <div class="card-body">
            <form action="<?php echo site_url('/Comunicados/EditarComunicado') ?>" method="post" id="frm_nuevo_comunicado">
            <div class="col-4">
                        <div class="mb-3">
                            <input type="hidden"  value="<?php echo $comunicadoEditar->id_com ?>" type="text" class="form-control" name="id_com" id="id_com" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                <div class="row justify-content-center align-items-center g-2">
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="fecha_com" class="form-label">FECHA:</label>
                            <input  value="<?php echo $comunicadoEditar->fecha_com ?>" type="date" class="form-control" name="fecha_com" id="fecha_com" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                    
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="mensaje_com" class="form-label">MENSAJE:</label>
                            <input value="<?php echo $comunicadoEditar->mensaje_com ?>" type="text" class="form-control" name="mensaje_com" id="mensaje_com" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="actualizacion_com" class="form-label">ACTUALIZACION:</label>
                            <input value="<?php echo $comunicadoEditar->actualizacion_com ?>" type="datetime-local" class="form-control" name="actualizacion_com" id="actualizacion_com" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                    
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="creacion_com" class="form-label">CREACION:</label>
                            <input value="<?php echo $comunicadoEditar->creacion_com ?>" type="datetime-local" class="form-control" name="creacion_com" id="creacion_com" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                    </div>
                    <center>
                        <button  class="btn btn-primary" type="submit">Guardar</button>
                        <a name="" id="" class="btn btn-dark" href="<?php echo site_url('/Comunicados/index') ?>" role="button">Cancelar</a>
                    </center>
            </form>
        </div>
    </div>
</div>
</div>
</div>

<script type="text/javascript">
    $("#frm_nuevo_comunicado").validate({
        rules: {
            fecha_com: {
                required: true,
                minlength: 10,
                maxlength: 10,
                digits: true
            },
            mensaje_com: {
                required: true,

                letras: true
            },
            actualizacion_com: {
                required: true,

                letras: true
            },
            


        },
        messages: {
            fecha_com: {
                required: "Nesesariamente debe elegir una",
                minlength: "Cedula incorrecta",
                maxlength: "Cedula incorrecta 10 digitos",
                digits: "Este campo solo acepta numeros",
                number: "Este campo solo acepta numeros",
            },
            mensaje_com: {
                required: "Porfavor ingrese la descripcion del delito",
                // minlength: "El apellido debe tener minimo 3 digitos ",
                // maxlength: "APellido incorrecto"
            },
            actualizacion_com: {
                required: "debe ingresar el tiempo de condena para el delito",
                // minlength: "El nombre debe tener 3 dígitos",
                // maxlength: "Nombres incorrectos 50 dígitos",
            },
            


        }
    });
</script>

</div>