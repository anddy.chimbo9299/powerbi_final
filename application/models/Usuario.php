<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario extends CI_Model {
    public function __construct() {
    parent::__construct(); 
    }

    public function insertar($datos){
        return $this->db->insert("usuario",$datos);
    }

    public function obtenerTodos(){
        $listadoUsuario=
        $this->db->get("usuario");
        if($listadoUsuario
           ->num_rows()>0){//Si hay datos
           return $listadoUsuario->result();
        }else{//No hay datos
           return false;
        } 
    }

    public function eliminar($id_usu){
        $this->db->where("id_usu",$id_usu);
  	return $this->db->delete("usuario");
    }

    public function obtenerPorId($id_usu){
        $this->db->where("id_usu",$id_usu);
        $usuario=$this->db->get("usuario");
        if($usuario->num_rows()>0){
          return $usuario->row();
        }
        return false;  
    }

}