<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Evento extends CI_Model {
    public function __construct() {
    parent::__construct();
    }

    public function insertar($datos){
        return $this->db->insert("evento",$datos);
    }

    public function obtenerTodos(){
        $eventoDetalle=
        $this->db->get("evento");
        if($eventoDetalle
           ->num_rows()>0){//Si hay datos
           return $eventoDetalle->result();
        }else{//No hay datos
           return false;
        }
    }

    public function eliminar($id_eve){
        $this->db->where("id_eve",$id_eve);
  	return $this->db->delete("evento");
    }

    public function obtenerPorId($id_eve){
        $this->db->where("id_eve",$id_eve);
        $evento=$this->db->get("evento");
        if($evento->num_rows()>0){
          return $evento->row();
        }
        return false;
    }

    public function actualizar($id_eve,$datos)
    {
      $this->db->where("id_eve",$id_eve);
      return $this->db->update("evento",$datos);
    }

}
