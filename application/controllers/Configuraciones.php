<?php
class Configuraciones extends CI_Controller{

    function __construct(){
        parent::__construct();
        $this->load->model('Configuracion');
    }
    public function index(){
        $datos['configuracionesIndex']=$this->Configuracion->ObtenerConfiguraciones();
        $this->load->view('header');
        $this->load->view('/configuraciones/index',$datos);
        $this->load->view('footer');
    }
    public function nuevo(){
        $this->load->view('header');
        $this->load->view('/configuraciones/nuevo');
        $this->load->view('footer');
    }
    public function editar($id_con){
        $datos['configuracionEditar']=$this->Configuracion->obtenerConfiguracion($id_con);
        $this->load->view('header');
        $this->load->view('/configuraciones/editar',$datos);
        $this->load->view('footer');
    }
    public function GuardarConfiguracion(){
        $datosConfiguracion=array(
			       "nombre_con"=>$this->input->post('nombre_con'),
			       "ruc_con"=>$this->input->post('ruc_con'),
      		   "logo_con"=>$this->input->post('logo_con'),
			       "telefono_con"=>$this->input->post('telefono_con'),
      		   "direccion_con"=>$this->input->post('direccion_con'),
      		   "email_con"=>$this->input->post('email_con'),
      		  "servidor_con"=>$this->input->post('servidor_con'),
            "puerto_con"=>$this->input->post('puerto_con'),
            "password_con"=>$this->input->post('password_con'),
            "creacion_con"=>$this->input->post('creacion_con'),
            "actualizacion_con"=>$this->input->post('actualizacion_con'),
            "anio_inicial_con"=>$this->input->post('anio_inicial_con'),
            "mes_inicial_con"=>$this->input->post('mes_inicial_con'),

		);
        if ($this->Configuracion->InsertarConfiguracion($datosConfiguracion)) {

        } else {

        }
        redirect("/Configuraciones/index");

    }
    public function EditarConfiguracion(){
        $nuevosDatos=array(
            "nombre_con"=>$this->input->post('nombre_con'),
			"ruc_con"=>$this->input->post('ruc_con'),
      		"logo_con"=>$this->input->post('logo_con'),
			"telefono_con"=>$this->input->post('telefono_con'),
      		"direccion_con"=>$this->input->post('direccion_con'),
      		"email_con"=>$this->input->post('email_con'),
      		"servidor_con"=>$this->input->post('servidor_con'),
            "puerto_con"=>$this->input->post('puerto_con'),
            "password_con"=>$this->input->post('password_con'),
            "creacion_con"=>$this->input->post('creacion_con'),
            "actualizacion_con"=>$this->input->post('actualizacion_con'),
            "anio_inicial_con"=>$this->input->post('anio_inicial_con'),
            "mes_inicial_con"=>$this->input->post('mes_inicial_con')
        );
        // print_r($nuevosDatos);
        $id_con = $this->input->post('id_con');
        if ($this->Configuracion->actualizarConfiguracion($id_con,$nuevosDatos)) {
            redirect("/Configuraciones/index");
        } else {
            echo "No se pudo actualizar";
        }


    }
    public function Eliminar($id_con){
        if ($this->Configuracion->borrarConfiguracion($id_con)) {

        } else {

        }
        redirect("/Configuraciones/index");

}
}





?>
