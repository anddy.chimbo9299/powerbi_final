<div class="row">
    <div class="col-md-6">
        <h1>NuevO Historial</h1>
    </div>
    <div>
        <a href="<?php echo site_url();?>historiales/nuevo">Agregar Nuevo </a>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <form action="<?php echo site_url(); ?>/historiales/procesoActualizar" method="post">
            <input type="text" id="id_his" name="id_his" value="<?php echo $historialEditar->id_his?>">
            <div class="row">
                <div  class="col-md-4">
                    <label for=""> Medidor:</label>
                    <br>
                    <select name="fk_id_med" id="fk_id_med" class="form-control">
                        <?php  foreach ($consumo as $t) { ?>
                            <option value="<?= $t->id_lec?>"><?= $t->id_lec?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-md-4">
                    <label for="">Nombre Socio:</label>
                    <br>
                    <select name="fk_id_soc" id="fk_id_soc" class="form-control">
                        <?php  foreach ($socios as $t) { ?>
                            <option value="<?= $t->id_soc?>"><?= $t->nombres_soc?></option>
                        <?php } ?>
                    </select>

                    </div>
                <div class="col-md-4">
                    <label for=""> Estado:</label>
                    <br>
                    <input type="text" class="form-control"name="estado_his" value="<?php echo $historialEditar->estado_his?>" id="estado_his" placeholder="Ingrese el estado">
                </div>
              
            </div>
            <div class="row">
                <div  class="col-md-6">
                    <label for=""> Observacion:</label>
                    <br>
                    <input type="text" class="form-control" name="observacion_his" value="<?php echo $historialEditar->observacion_his?>" id="observacion_his" placeholder="Ingrese la observacion">

                </div>
                 <div  class="col-md-6">
                    <label for=""> Propietario :</label>
                    <br>
                    <input type="text" class="form-control" name="propietario_actual_his" value="<?php echo $historialEditar->propietario_actual_his?>" id="propietario_actual_his" placeholder="Ingrese el propietario">

                </div>



            </div>
            <br>
            <br>

            <div class="row">
                <div class="col-md-12 text-center">
                    <button type="submit" name="button" class="btn btn-primary">Guardar</button>&nbsp;
                    <a href="<?php echo site_url(); ?>/historiales/index" class="btn btn-danger">Cancelar </a>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
    $("#fk_id_soc").val('<?php echo $historialEditar->fk_id_soc; ?>');

</script>
