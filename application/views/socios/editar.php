<h1>Editar Socio</h1>
<div class="row">
    <div class="col-md-12">
        <form action="<?php echo site_url(); ?>/socios/procesoActualizar" method="post">
            
            <input type="hidden" name="id_soc" id="id_soc"  value="<?php echo $socioEditar->id_soc?>"/>
            <div class="row">
                <div  class="col-md-3">
                    <label for=""> Tipo de Socio :</label>
                    <br>
                    <input type="text" class="form-control" name="tipo_soc" value="<?php echo $socioEditar->tipo_soc?>" id="tipo_soc" placeholder="Ingrese el tipo de socio">
                </div>
                <div class="col-md-3">
                    <label for="">Nombre Socio:</label>
                    <br>
                    <input type="text" class="form-control" name="nombres_soc" value="<?php echo $socioEditar->nombres_soc?>" id="nombres_soc" placeholder="Ingrese su nombre">

                </div>
                <div class="col-md-3">
                    <label for=""> Identificacion:</label>
                    <br>
                    <input type="text" class="form-control"name="identificacion_soc" value="<?php echo $socioEditar->identificacion_soc?>" id="identificacion_soc" placeholder="Ingrese su Identificacion">
                </div>
                <div class="col-md-3">
                    <label for=""> Primer Apellido:</label>
                    <br>
                    <input type="text" class="form-control"name="primer_apellido_soc" value="<?php echo $socioEditar->primer_apellido_soc?>" id="primer_apellido_soc" placeholder="Ingrese su Apellido">
                </div>
               
               
            </div>
            <div class="row">
                <div  class="col-md-3">
                    <label for=""> Segundo Apellido:</label>
                    <br>
                    <input type="text" class="form-control" name="segundo_apellido_soc" value="<?php echo $socioEditar->segundo_apellido_soc?>" id="segundo_apellido_soc" placeholder="Ingrese su Apellido">
                </div>
                <div  class="col-md-3">
                    <label for=""> Email :</label>
                    <br>
                    <input type="text" class="form-control" name="email_soc" value="<?php echo $socioEditar->email_soc?>" id="email_soc" placeholder="Ingrese el email">
                </div>
                <div class="col-md-3">
                    <label for="">Telefono:</label>
                    <br>
                    <input type="text" class="form-control"name="telefono_soc" id="telefono_soc" value="<?php echo $socioEditar->telefono_soc?>"  placeholder="Ingrese el telefono">

                </div>
                <div class="col-md-3">
                    <label for="">Direccion:</label>
                    <br>
                    <input type="text" class="form-control"name="direccion_soc" id="direccion_soc" value="<?php echo $socioEditar->direccion_soc?>"  placeholder="Ingrese la direccion">

                </div>
                
                
               
            </div>
            <div class="row">   
                <div class="col-md-3">
                    <label for="">Fecha de Nacimiento:</label>
                    <br>
                    <input type="date" class="form-control"name="fecha_nacimiento_soc" id="fecha_nacimiento_soc" value="<?php echo $socioEditar->fecha_nacimiento_soc?>"  placeholder="Ingrese la direccion">

                </div> 
                <div class="col-md-3">
                    <label for="">Tiene discapacidad:</label>
                    <br>
                    <select name="discapacidad_soc" id="discapacidad_soc" class="form-control">
                        <option value="Si">Si</option>
                        <option value="No">No</option>
                    </select>
                </div>        
                <div class="col-md-3">
                    <label for="">Tipo Usuario:</label>
                    <br>
                    <select name="fk_id_usu" id="fk_id_usu" class="form-control">
                        <?php  foreach ($usuario as $t) { ?>
                            <option value="<?= $t->id_usu?>"><?= $t->nombre_usu?></option>
                        <?php } ?>
                    </select>                    
                </div>
                <div class="col-md-3">
                    <label for="">Estado:</label>
                    <br>
                    <select name="estado_soc" id="estado_soc" class="form-control">
                        <option value="ACTIVO">Activo</option>
                        <option value="INACTIVO">Inactivo</option>
                    </select>

                </div>
            </div>
            <br>
            <br>
            <div class="row">
                <div class="col-md-12 text-center">
                    <button type="submit" name="button" class="btn btn-primary">Guardar</button>&nbsp;
                    <a href="<?php echo site_url(); ?>/socios/index" class="btn btn-danger">Cancelar </a>
                </div>
            </div>   
        </form>
    </div>
</div>

<script>
    $("#fk_id_usu").val('<?php echo $socioEditar->fk_id_usu; ?>');
    $("#discapacidad_soc").val('<?php echo $socioEditar->discapacidad_soc; ?>');
    $("#estado_soc").val('<?php echo $socioEditar->estado_soc; ?>');
</script>