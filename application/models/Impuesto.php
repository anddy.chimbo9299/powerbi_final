<?php 
class Impuesto extends CI_Model{
    function __construct(){
        parent::__construct();
    }
    public function InsertarImpuesto($datosImpuesto){
        return $this->db->insert('impuesto',$datosImpuesto);
    }
    public function ObtenerImpuestos(){
        $datosImpuestos = $this->db->get('impuesto');
        if ($datosImpuestos->num_rows()>0) {
            return $datosImpuestos->result();
        } else {
            return false;
        }
        
    }
    function obtenerImpuesto($id_imp){
        $this->db->where("id_imp",$id_imp);
        $impuesto = $this->db->get("impuesto");
        if ($impuesto->num_rows()) {
            return $impuesto->row();

        }
        return false;

    }
    public function actualizarImpuesto($id_imp,$datos){
        $this->db->where('id_imp',$id_imp);
        return $this->db->update("impuesto",$datos);
    }
    public function borrarImpuesto($id_imp){
        $this->db->where('id_imp',$id_imp);
        return $this->db->delete('impuesto');
        
    }

}

?>