<div class="container-fluid">
    <div class="card">

        <div class="card-body">
            <div class="row">
                <div class="col-6">
                    <br>

                    <h3><b>Lista de medidores</b></h3>
                </div>
                
                <div class="col-6 text-right">
                    <br>
                    <a name="" id="" class="btn btn-primary" href="<?php echo site_url("medidores/nuevo") ?>" role="button"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person-plus" viewBox="0 0 16 16">
                            <path d="M6 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6m2-3a2 2 0 1 1-4 0 2 2 0 0 1 4 0m4 8c0 1-1 1-1 1H1s-1 0-1-1 1-4 6-4 6 3 6 4m-1-.004c-.001-.246-.154-.986-.832-1.664C9.516 10.68 8.289 10 6 10s-3.516.68-4.168 1.332c-.678.678-.83 1.418-.832 1.664z" style="color: black;" />
                            <path fill-rule="evenodd" d="M13.5 5a.5.5 0 0 1 .5.5V7h1.5a.5.5 0 0 1 0 1H14v1.5a.5.5 0 0 1-1 0V8h-1.5a.5.5 0 0 1 0-1H13V5.5a.5.5 0 0 1 .5-.5" style="color: black;" />
                        </svg>&nbsp;&nbsp;&nbsp;Nuevo Medidor</a>
                </div>
            </div><br>
            <?php if ($medidors) { ?>
                <div class="table-responsive">
                    <table  id="tblmedidors">
                        <thead >
                            <tr>
                              <th>ID</th>
                              <th>Numero</th>
                              <th>Serie</th>
                              <th>Marca</th>
                              <th>Observacion</th>
                              <th>Estado</th>
                              <th>Fotografia</th>
                              <th>Creacion</th>
                              <th>Actulizar</th>
                              <th>Lectura</th>
                              <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($medidors as $filaTemporal) { ?>
                                <tr>
                                    <td><?php echo $filaTemporal->id_med ?></td>
                                    <td>
                                        <?php echo $filaTemporal->numero_med; ?> </td>
                                    <td>
                                        <?php echo $filaTemporal->serie_med; ?></td>
                                    <td>
                                        <?php echo $filaTemporal->marca_med; ?></td>
                                    <td>
                                        <?php echo $filaTemporal->observacion_med; ?></td>
                                    <td>
                                        <?php echo $filaTemporal->estado_med; ?></td>
                                    <td>
                                        <?php echo $filaTemporal->foto_med; ?></td>
                                    <td>
                                        <?php echo $filaTemporal->creacion_med; ?></td>
                                    <td>
                                        <?php echo $filaTemporal->actualizacion_med; ?></td>
                                    <td>
                                        <?php echo $filaTemporal->lectura_inicial_med; ?></td>

                                    <td class="text-center" >
                                        <a href="<?php echo site_url(); ?>/medidores/editar/<?php echo $filaTemporal->id_med; ?>" title="Editar Medidor" >
                                            <i class="mdi  mdi-pencil">Editar</i>
                                        </a>
                                        &nbsp;&nbsp;

                                        <a href="<?php echo site_url(); ?>/medidores/eliminar/<?php echo $filaTemporal->id_med; ?>" title="Borrar Medidor" style="color:red" onclick="return confirm('Esta seguro de borra el registro?');">
                                            <i class="mdi  mdi-close">Eliminar</i>
                                        </a>
            
                                    </td>
                                </tr>
                            <?php } ?>

                        </tbody>
                    </table>
                </div>
            <?php } ?>

        </div>
    </div>

</div>


<script type="text/javascript">
    $("#tblmedidors")
        .DataTable({
            language: {
                "decimal": "",
                "emptyTable": "No hay información",
                "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                "infoPostFix": "",
                "thousands": ",",
                "lengthMenu": "Mostrar _MENU_ Entradas",
                "loadingRecords": "Cargando...",
                "processing": "Procesando...",
                "search": "Buscar:",
                "zeroRecords": "Sin resultados encontrados",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                }
            },
        });
</script>

