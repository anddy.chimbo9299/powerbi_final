<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Socio extends CI_Model {
    public function __construct() {
    parent::__construct(); 
    }

    public function insertar($datos){
        return $this->db->insert("socio",$datos);
    }

    public function obtenerTodos(){
        $listadoSocio=
        $this->db->get("socio");
        if($listadoSocio
           ->num_rows()>0){//Si hay datos
           return $listadoSocio->result();
        }else{//No hay datos
           return false;
        } 
    }

    public function eliminar($id_soc){
        $this->db->where("id_soc",$id_soc);
  	return $this->db->delete("socio");
    }

    public function obtenerPorId($id_soc){
        $this->db->where("id_soc",$id_soc);
        $socio=$this->db->get("socio");
        if($socio->num_rows()>0){
          return $socio->row();
        }
        return false;  
    }

    public function actualizar($id_soc,$datos)
    {
      $this->db->where("id_soc",$id_soc);
      return $this->db->update("socio",$datos);
    }

}