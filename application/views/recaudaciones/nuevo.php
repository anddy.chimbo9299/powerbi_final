<h1>Nueva Recaudacion</h1>
<div class="row">
    <div class="col-md-12">
        <form action="<?php echo site_url(); ?>/recaudaciones/guardar" method="post">

            <div class="row">
                <div  class="col-md-3">
                    <label for=""> Numero de Factura :</label>
                    <br>
                    <input type="number" class="form-control" name="numero_factura_rec" value="" id="numero_factura_rec" placeholder="Ingrese el tipo de socio">
                </div>
                <div class="col-md-3">
                    <label for="">Numero de  Autorizacion:</label>
                    <br>
                    <input type="number" class="form-control" name="numero_autorizacion_rec" value="" id="numero_autorizacion_rec" placeholder="Ingrese su nombre">

                    </div>
                <div class="col-md-3">
                    <label for="">Fecha de Autorizacion:</label>
                    <br>
                    <input type="date" class="form-control"name="fecha_hora_autorizacion_rec" value="" id="fecha_hora_autorizacion_rec" placeholder="Ingrese su Identificacion">
                </div>    
                <div class="col-md-3">
                    <label for=""> Ambiente:</label>
                    <br>
                    <input type="text" class="form-control"name="ambiente_rec" value="" id="ambiente_rec" placeholder="Ingrese el ambiente">
                </div>
               
                
               
            </div>
            <div class="row">
                <div  class="col-md-3">
                    <label for=""> Emision:</label>
                    <br>
                    <input type="text" class="form-control" name="emision_rev" value="" id="emision_rev" placeholder="Ingrese la emision">
                </div>
                 <div  class="col-md-3">
                    <label for=""> Clave de Acceso :</label>
                    <br>
                    <input type="text" class="form-control" name="clave_acceso_rec" value="" id="clave_acceso_rec" placeholder="Ingrese el email">
                </div>
                <div class="col-md-3">
                    <label for="">Email:</label>
                    <br>
                    <input type="text" class="form-control"name="email_rec" id="email_rec" value=""  placeholder="Ingrese el email">

                </div>
                <div class="col-md-3">
                    <label for="">Observacion:</label>
                    <br>
                    <input type="text" class="form-control"name="observacion_rec" id="observacion_rec" value=""  placeholder="Ingrese la observacion">

                </div>
                
                
               
            </div>
            <div class="row">    
                <div class="col-md-3">
                    <label for="">Nombre Socio:</label>
                    <br>
                    <select name="fk_id_soc" id="fk_id_soc" class="form-control">
                        <?php  foreach ($socios as $t) { ?>
                            <option value="<?php echo $t->id_soc?>" > <?= $t->nombres_soc?></option>
                        <?php } ?>
                        
                    </select>
                </div>
                <div class="col-md-3">
                    <label for="">Nombre Recibo:</label>
                    <br>
                    <input type="text" class="form-control" name="nombre_rec" id="nombre_rec" placeholder="Ingrese el nombre del Recibo" >
                </div>        
                <div class="col-md-3">
                    <label for="">Identificacion:</label>
                    <br>
                    <input type="text" class="form-control" name="identificacion_rec" id="identificacion_rec" placeholder="Ingrese la identifiacion" >
            
                </div>
                <div class="col-md-3">
                    <label for="">Direccion:</label>
                    <br>
                    <input type="text" class="form-control" name="direccion_rec" id="direccion_rec" placeholder="Ingrese la direccion" >
            
                </div>
              
            </div>
            <div class= "row">  
                
                <div class="col-md-3">
                    <label for="">Fechas Emision:</label>
                    <br>
                    <input type="date" class="form-control" name="fecha_emision_rec" id="fecha_emision_rec" placeholder="Ingrese la direccion" >
            
                </div>
                <div class="col-md-3">
                    <label for="">Estado:</label>
                    <br>
                    <select name="estado_rec" id="estado_rec" class="form-control">
                    
                        <option value="ABIERTO">ABIERTO</option>
                        <option value="CERRADO">CERRADO</option>
                        
                    </select>

                </div>
            </div>
            <br>
            <br>
            
            <div class="row">
                <div class="col-md-12 text-center">
                    <button type="submit" name="button" class="btn btn-primary">Guardar</button>&nbsp;
                    <a href="<?php echo site_url(); ?>/recaudaciones/index" class="btn btn-danger">Cancelar </a>
                </div>
            </div>   
        </form>
    </div>
</div>