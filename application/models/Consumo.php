<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Consumo extends CI_Model {
    public function __construct() {
    parent::__construct(); 
    }

    public function insertar($datos){
        return $this->db->insert("consumo",$datos);
    }

    public function obtenerTodos(){
        $listadoConsumo=
        $this->db->get("consumo");
        if($listadoConsumo
           ->num_rows()>0){//Si hay datos
           return $listadoConsumo->result();
        }else{//No hay datos
           return false;
        } 
    }

    public function eliminar($id_consumo){
        $this->db->where("id_consumo",$id_consumo);
  	return $this->db->delete("consumo");
    }

    public function obtenerPorId($id_consumo){
        $this->db->where("id_consumo",$id_consumo);
        $consumo=$this->db->get("consumo");
        if($consumo->num_rows()>0){
          return $consumo->row();
        }
        return false;  
    }

    public function actualizar($id_consumo,$datos)
    {
      $this->db->where("id_consumo",$id_consumo);
      return $this->db->update("consumo",$datos);
    }

}