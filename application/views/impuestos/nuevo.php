<div class="container-fluid">
    <div class="card">
        <center>
            <h1>
                <b>
                    FORMULARIO INGRESO NUEVO IMPUESTO
                </b>
            </h1>
        </center>
        <div class="card-body">
            <form action="<?php echo site_url('/Impuestos/GuardarImpuesto') ?>" method="post" id="frm_nuevo_impuesto">
                <div class="row justify-content-center align-items-center g-2">

                    <div class="col-md-4">
                        <div class="mb-3">
                            <label for="nombre_imp" class="form-label">NOMBRE:</label>
                            <input  type="text" class="form-control" name="nombre_imp" id="nombre_imp" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="mb-3">
                            <label for="descripcion_imp" class="form-label">DESCRIPCION:</label>
                            <input  type="text" class="form-control" name="descripcion_imp" id="descripcion_imp" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                    
                    <div class="col-md-4">
                        <div class="mb-3">
                            <label for="porcentaje_imp" class="form-label">PORCENTAJE:</label>
                            <input type="number" step="0.01" inputmode="decimal" class="form-control" name="porcentaje_imp" id="porcentaje_imp" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="mb-3">
                            <label for="retencion_imp" class="form-label">RETENCION:</label>
                            <input  type="text" class="form-control" name="retencion_imp" id="retencion_imp" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="mb-3">
                            <label for="estado_imp" class="form-label">ESTADO:</label>
                            <input  type="text" class="form-control" name="estado_imp" id="estado_imp" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="mb-3">
                            <label for="creacion_imp" class="form-label">CREACION:</label>
                            <input  type="datetime-local" class="form-control" name="creacion_imp" id="creacion_imp" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="mb-3">
                            <label for="actualizacion_imp" class="form-label">ACTUALIZACION:</label>
                            <input type="datetime-local" class="form-control" name="actualizacion_imp" id="actualizacion_imp" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                </div>
                <!-- Nueva fila para los botones -->
                <div class="row justify-content-center align-items-center g-2">
                    <div class="col-md-12 text-center">
                        <button class="btn btn-primary" type="submit">Guardar</button>
                        <a name="" id="" class="btn btn-dark" href="<?php echo site_url('/Impuestos/index') ?>" role="button">Cancelar</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    $("#frm_nuevo_impuesto").validate({
        rules: {
            nombre_imp: {
                required: true,
                minlength: 10,
                maxlength: 10,
                digits: true
            },
            descripcion_imp: {
                required: true,

                letras: true
            },
            porcentaje_imp: {
                required: true,

                letras: true
            },
            


        },
        messages: {
            nombre_imp: {
                required: "Nesesariamente debe elegir una",
                minlength: "Cedula incorrecta",
                maxlength: "Cedula incorrecta 10 digitos",
                digits: "Este campo solo acepta numeros",
                number: "Este campo solo acepta numeros",
            },
            descripcion_imp: {
                required: "Porfavor ingrese la descripcion del delito",
                // minlength: "El apellido debe tener minimo 3 digitos ",
                // maxlength: "APellido incorrecto"
            },
            porcentaje_imp: {
                required: "debe ingresar el tiempo de condena para el delito",
                // minlength: "El nombre debe tener 3 dígitos",
                // maxlength: "Nombres incorrectos 50 dígitos",
            },
            


        }
    });
</script>

</div>
