<?php 
class Impuestos extends CI_Controller{

    function __construct(){
        parent::__construct();
        $this->load->model('Impuesto');
    }
    public function index(){
        $datos['impuestosIndex']=$this->Impuesto->ObtenerImpuestos();
        $this->load->view('header');
        $this->load->view('/impuestos/index',$datos);
        $this->load->view('footer');
    }
    public function nuevo(){
        $this->load->view('header');
        $this->load->view('/impuestos/nuevo');
        $this->load->view('footer');
    }
    public function editar($id_imp){
        $datos['impuestoEditar']=$this->Impuesto->obtenerImpuesto($id_imp);
        $this->load->view('header');
        $this->load->view('/impuestos/editar',$datos);
        $this->load->view('footer');
    }
    public function GuardarImpuesto(){
        $datosImpuesto=array(
			"nombre_imp"=>$this->input->post('nombre_imp'),
			"descripcion_imp"=>$this->input->post('descripcion_imp'),
      		"porcentaje_imp"=>$this->input->post('porcentaje_imp'),
			"retencion_imp"=>$this->input->post('retencion_imp'),
      		"estado_imp"=>$this->input->post('estado_imp'),
      		"creacion_imp"=>$this->input->post('creacion_imp'),
      		"actualizacion_imp"=>$this->input->post('actualizacion_imp')
		);
        if ($this->Impuesto->InsertarImpuesto($datosImpuesto)) {

        } else {

        }
        redirect("/Impuestos/index");
        
    }
    public function EditarImpuesto(){
        $nuevosDatos=array(
            "nombre_imp"=>$this->input->post('nombre_imp'),
			"descripcion_imp"=>$this->input->post('descripcion_imp'),
      		"porcentaje_imp"=>$this->input->post('porcentaje_imp'),
			"retencion_imp"=>$this->input->post('retencion_imp'),
      		"estado_imp"=>$this->input->post('estado_imp'),
      		"creacion_imp"=>$this->input->post('creacion_imp'),
      		"actualizacion_imp"=>$this->input->post('actualizacion_imp')
        );
        // print_r($nuevosDatos);
        $id_imp = $this->input->post('id_imp');
        if ($this->Impuesto->actualizarImpuesto($id_imp,$nuevosDatos)) {
            redirect("/Impuestos/index");
        } else {
            echo "No se pudo actualizar";
        }
        

    }
    public function Eliminar($id_imp){
        if ($this->Impuesto->borrarImpuesto($id_imp)) {

        } else {

        }
        redirect("/Impuestos/index");
        
}  
}





?>