<h1>Nuevo Detalle</h1>
<div class="row">
    <div class="col-md-12">
        <form action="<?php echo site_url(); ?>/detalles/guardar" method="post">

            <div class="row">
                <div  class="col-md-3">
                    <label for=""> Lectura:</label>
                    <br>
                    <select name="fk_id_lec" id="fk_id_lec" class="form-control">
                        <?php  foreach ($lectura as $t) { ?>
                            <option value="<?= $t->id_lec?>"><?= $t->id_lec?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-md-3">
                    <label for="">Recaudacion:</label>
                    <br>
                    <select name="fk_id_rec" id="fk_id_rec" class="form-control">
                        <?php  foreach ($recaudacion as $t) { ?>
                            <option value="<?= $t->id_rec?>"><?= $t->id_rec?></option>
                        <?php } ?>
                    </select>

                    </div>
                <div class="col-md-3">
                    <label for=""> Cantidad:</label>
                    <br>
                    <input type="text" class="form-control"name="cantidad_det" value="" id="cantidad_det" placeholder="Ingrese la cantidad">
                </div>    
                
                             
            </div>
            <div class="row">
                <div  class="col-md-3">
                    <label for=""> Detalle:</label>
                    <br>
                    <input type="text" class="form-control" name="detalle_det" value="" id="detalle_det" placeholder="Ingrese el detalle">
                </div>
                 <div  class="col-md-3">
                    <label for=""> Valor Unitario :</label>
                    <br>
                    <input type="text" class="form-control" name="valor_unitario_det" value="" id="valor_unitario_det" placeholder="Ingrese el valor unitario">
                </div>
                <div class="col-md-3">
                    <label for="">Subtotal:</label>
                    <br>
                    <input type="text" class="form-control"name="subtotal_det" id="subtotal_det" value=""  placeholder="Ingrese el subtotal">

                </div>
                <div class="col-md-3">
                    <label for="">Iva:</label>
                    <br>
                    <input type="text" class="form-control"name="iva_det" id="iva_det" value=""  placeholder="Ingrese el IVA">

                </div>
                                              
            </div>                
            <br>
            <br>
            
            <div class="row">
                <div class="col-md-12 text-center">
                    <button type="submit" name="button" class="btn btn-primary">Guardar</button>&nbsp;
                    <a href="<?php echo site_url(); ?>/detalles/index" class="btn btn-danger">Cancelar </a>
                </div>
            </div>   
        </form>
    </div>
</div>