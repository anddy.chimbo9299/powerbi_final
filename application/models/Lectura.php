<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lectura extends CI_Model {
    public function __construct() {
    parent::__construct(); 
    }

    public function insertar($datos){
        return $this->db->insert("lectura",$datos);
    }

    public function obtenerTodos(){
        $listadoLectura=
        $this->db->get("lectura");
        if($listadoLectura
           ->num_rows()>0){//Si hay datos
           return $listadoLectura->result();
        }else{//No hay datos
           return false;
        } 
    }

    public function eliminar($id_lec){
        $this->db->where("id_lec",$id_lec);
  	return $this->db->delete("lectura");
    }

    public function obtenerPorId($id_lec){
        $this->db->where("id_lec",$id_lec);
        $lectura=$this->db->get("lectura");
        if($lectura->num_rows()>0){
          return $lectura->row();
        }
        return false;  
    }

    public function actualizar($id_lec,$datos)
    {
      $this->db->where("id_lec",$id_lec);
      return $this->db->update("lectura",$datos);
    }

}