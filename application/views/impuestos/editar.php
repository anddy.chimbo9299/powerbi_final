<div class="container-fluid">
    <div class="card">
        <center>
            <h1>
                <b>
                    FORMULARIO EDITAR NUEVO IMPUESTO
                </b>
            </h1>
        </center>
        <div class="card-body">
            <form action="<?php echo site_url('/Impuestos/EditarImpuesto') ?>" method="post" id="frm_nuevo_impuesto">
            <div class="col-4">
                        <div class="mb-3">
                            <input type="hidden"  value="<?php echo $impuestoEditar->id_imp ?>" type="text" class="form-control" name="id_imp" id="id_imp" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                <div class="row justify-content-center align-items-center g-2">
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="nombre_imp" class="form-label">NOMBRE:</label>
                            <input  value="<?php echo $impuestoEditar->nombre_imp ?>" type="text" class="form-control" name="nombre_imp" id="nombre_imp" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                    
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="descripcion_imp" class="form-label">DESCRIPCION:</label>
                            <input value="<?php echo $impuestoEditar->descripcion_imp ?>" type="text" class="form-control" name="descripcion_imp" id="descripcion_imp" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="porcentaje_imp" class="form-label">PORCENTAJE:</label>
                            <input value="<?php echo number_format($impuestoEditar->porcentaje_imp, 2, '.', ''); ?>" type="text" class="form-control" name="porcentaje_imp" id="porcentaje_imp" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="retencion_imp" class="form-label">RETENCION:</label>
                            <input value="<?php echo $impuestoEditar->retencion_imp ?>" type="text" class="form-control" name="retencion_imp" id="retencion_imp" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="estado_imp" class="form-label">ESTADO:</label>
                            <input value="<?php echo $impuestoEditar->estado_imp ?>" type="text" class="form-control" name="estado_imp" id="estado_imp" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="creacion_imp" class="form-label">CREACION:</label>
                            <input value="<?php echo $impuestoEditar->creacion_imp ?>" type="datetime-local" class="form-control" name="creacion_imp" id="creacion_imp" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="actualizacion_imp" class="form-label">ACTUALIZACION:</label>
                            <input value="<?php echo $impuestoEditar->actualizacion_imp ?>" type="datetime-local" class="form-control" name="actualizacion_imp" id="actualizacion_imp" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                    <center>
                        <button  class="btn btn-primary" type="submit">Guardar</button>
                        <a name="" id="" class="btn btn-dark" href="<?php echo site_url('/Impuestos/index') ?>" role="button">Cancelar</a>
                    </center>
            </form>
        </div>
    </div>
</div>
</div>
</div>
<script type="text/javascript">
    $("#frm_nuevo_impuesto").validate({
        rules: {
            nombre_imp: {
                required: true,
                minlength: 10,
                maxlength: 10,
                digits: true
            },
            descripcion_imp: {
                required: true,

                letras: true
            },
            porcentaje_imp: {
                required: true,

                letras: true
            },
            


        },
        messages: {
            nombre_imp: {
                required: "Nesesariamente debe elegir una",
                minlength: "Cedula incorrecta",
                maxlength: "Cedula incorrecta 10 digitos",
                digits: "Este campo solo acepta numeros",
                number: "Este campo solo acepta numeros",
            },
            descripcion_imp: {
                required: "Porfavor ingrese la descripcion del delito",
                // minlength: "El apellido debe tener minimo 3 digitos ",
                // maxlength: "APellido incorrecto"
            },
            porcentaje_imp: {
                required: "debe ingresar el tiempo de condena para el delito",
                // minlength: "El nombre debe tener 3 dígitos",
                // maxlength: "Nombres incorrectos 50 dígitos",
            },
            


        }
    });
</script>

</div>