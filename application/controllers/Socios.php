<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Socios extends CI_Controller {

	public function __construct() {
		parent::__construct(); 
		$this->load->model("Socio"); 
        $this->load->model("Usuario");  
	}

	public function index()
	{
		$data["socios"]=$this->Socio->obtenerTodos();
		$this->load->view('header');
		$this->load->view('socios/index',$data);
		$this->load->view('footer');
	}

    public function nuevo()
	{
        $data["usuario"]=$this->Usuario->obtenerTodos();
		$this->load->view('header');
		$this->load->view('socios/nuevo',$data);
		$this->load->view('footer');
	}

    public function editar($id_soc)
	{
        $data["usuario"]=$this->Usuario->obtenerTodos();
        $data["socioEditar"] = $this->Socio->obtenerPorId($id_soc);
		$this->load->view('header');
		$this->load->view('socios/editar',$data);
		$this->load->view('footer');
	}

	public function guardar(){
		$datos = array(
            "tipo_soc"=>$this->input->post("tipo_soc"),
			"nombres_soc"=>$this->input->post("nombres_soc"),
            "identificacion_soc"=>$this->input->post("identificacion_soc"),
			"primer_apellido_soc"=>$this->input->post("primer_apellido_soc"),
            "segundo_apellido_soc"=>$this->input->post("segundo_apellido_soc"),
            "email_soc"=> $this->input->post("email_soc"),
            "telefono_soc"=>$this->input->post("telefono_soc"),
            "direccion_soc"=>$this->input->post("direccion_soc"),
            "fecha_nacimiento_soc"=>$this->input->post("fecha_nacimiento_soc"),
            "discapacidad_soc"=>$this->input->post("discapacidad_soc"),
            "fk_id_usu"=>$this->input->post("fk_id_usu"),
			"estado_soc"=>$this->input->post("estado_soc")

		);
		//validamos los campos del formulario 
		if ($this->Socio->insertar($datos))
		{
			//llamamos la libraria session user_data solo para una vez y flash_data para varias
			
		}else {
			
		}
		redirect ('socios/index');

	}

	public function eliminar($id_soc){
	
		  if ($this->Socio->eliminar($id_soc)) {
			
		  } else {
			
	 
		  }redirect ('socios/index');
	}

    public function procesoActualizar(){
        $datos = array(
            "tipo_soc"=>$this->input->post("tipo_soc"),
			"nombres_soc"=>$this->input->post("nombres_soc"),
            "identificacion_soc"=>$this->input->post("identificacion_soc"),
			"primer_apellido_soc"=>$this->input->post("primer_apellido_soc"),
            "segundo_apellido_soc"=>$this->input->post("segundo_apellido_soc"),
            "email_soc"=> $this->input->post("email_soc"),
            "telefono_soc"=>$this->input->post("telefono_soc"),
            "direccion_soc"=>$this->input->post("direccion_soc"),
            "fecha_nacimiento_soc"=>$this->input->post("fecha_nacimiento_soc"),
            "discapacidad_soc"=>$this->input->post("discapacidad_soc"),
            "fk_id_usu"=>$this->input->post("fk_id_usu"),
			"estado_soc"=>$this->input->post("estado_soc")

		);
        $id_soc=$this->input->post("id_soc");
		//validamos los campos del formulario 
		if ($this->Socio->actualizar($id_soc,$datos))
		{
			//llamamos la libraria session user_data solo para una vez y flash_data para varias
			
		}else {
			
		}
		redirect ('socios/index');

    }
}
