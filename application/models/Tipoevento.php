<?php 
class TipoEvento extends CI_Model{
    function __construct(){
        parent::__construct();
    }
    public function InsertarTipoEvento($datosTipoEvento){
        return $this->db->insert('tipo_evento',$datosTipoEvento);
    }
    public function ObtenerTiposEventos(){
        $datosTiposEventos = $this->db->get('tipo_evento');
        if ($datosTiposEventos->num_rows()>0) {
            return $datosTiposEventos->result();
        } else {
            return false;
        }
        
    }
    function obtenerTipoEvento($id_te){
        $this->db->where("id_te",$id_te);
        $tipoevento = $this->db->get("tipo_evento");
        if ($tipoevento->num_rows()) {
            return $tipoevento->row();

        }
        return false;

    }
    public function actualizarTipoEvento($id_te,$datos){
        $this->db->where('id_te',$id_te);
        return $this->db->update("tipo_evento",$datos);
    }
    public function borrarTipoEvento($id_te){
        $this->db->where('id_te',$id_te);
        return $this->db->delete('tipo_evento');
        
    }

}

?>