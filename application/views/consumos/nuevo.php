<div class="row">
    <div class="col-md-6">
        <h1>Nuevo Consumo</h1>
    </div>
    <div>
        <a href="<?php echo site_url();?>/consumos/nuevo">Agregar Nuevo </a>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <form action="<?php echo site_url(); ?>/consumos/guardar" method="post">

            <div class="row">
                <div  class="col-md-4">
                    <label for=""> Año:</label>
                    <br>
                    <input type="text" class="form-control"name="anio_consumo" value="" id="anio_consumo" placeholder="Ingrese el año">

                </div>
                <div class="col-md-4">
                    <label for="">Mes:</label>
                    <br>
                    <input type="text" class="form-control"name="mes_consumo" value="" id="mes_consumo" placeholder="Ingrese el mes">


                    </div>
                <div class="col-md-4">
                    <label for=""> Estado:</label>
                    <br>
                    <input type="text" class="form-control"name="estado_consumo" value="" id="estado_consumo" placeholder="Ingrese el estado">
                </div>    
                
                
                             
            </div>
            <div class="row">
                <div  class="col-md-6">
                    <label for=""> Numero de Mes de consumo:</label>
                    <br>
                    <input type="text" class="form-control" name="numero_mes_consumo" value="" id="numero_mes_consumo" placeholder="Ingrese la lectura actual">
                    
                </div>
                
                <div  class="col-md-6">
                    <label for=""> Fecha de Vencimiento:</label>
                    <br>
                    <input type="date" class="form-control" name="fecha_vencimiento_consumo" value="" id="fecha_vencimiento_consumo" placeholder="Ingrese la lectura actual">
                    
                </div>
                
                                              
            </div>                
            <br>
            <br>
            
            <div class="row">
                <div class="col-md-12 text-center">
                    <button type="submit" name="button" class="btn btn-primary">Guardar</button>&nbsp;
                    <a href="<?php echo site_url(); ?>/consumos/index" class="btn btn-danger">Cancelar </a>
                </div>
            </div>   
        </form>
    </div>
</div>