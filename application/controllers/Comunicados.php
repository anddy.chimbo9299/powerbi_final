<?php 
class Comunicados extends CI_Controller{

    function __construct(){
        parent::__construct();
        $this->load->model('Comunicado');
    }
    public function index(){
        $datos['comunicadosIndex']=$this->Comunicado->ObtenerComunicados();
        $this->load->view('header');
        $this->load->view('/comunicados/index',$datos);
        $this->load->view('footer');
    }
    public function nuevo(){
        $this->load->view('header');
        $this->load->view('/comunicados/nuevo');
        $this->load->view('footer');
    }
    public function editar($id_com){
        $datos['comunicadoEditar']=$this->Comunicado->obtenerComunicado($id_com);
        $this->load->view('header');
        $this->load->view('/comunicados/editar',$datos);
        $this->load->view('footer');
    }
    public function GuardarComunicado(){
        $datosComunicado=array(
			"fecha_com"=>$this->input->post('fecha_com'),
			"mensaje_com"=>$this->input->post('mensaje_com'),
      		"actualizacion_com"=>$this->input->post('actualizacion_com'),
            "creacion_com"=>$this->input->post('creacion_com'),
      		
		);
        if ($this->Comunicado->InsertarComunicado($datosComunicado)) {

        } else {

        }
        redirect("/Comunicados/index");
        
    }
    public function EditarComunicado(){
        $nuevosDatos=array(
            "fecha_com"=>$this->input->post('fecha_com'),
			"mensaje_com"=>$this->input->post('mensaje_com'),
      		"actualizacion_com"=>$this->input->post('actualizacion_com'),
            "creacion_com"=>$this->input->post('creacion_com'),
        );
        // print_r($nuevosDatos);
        $id_com = $this->input->post('id_com');
        if ($this->Comunicado->actualizarComunicado($id_com,$nuevosDatos)) {
            redirect("/Comunicados/index");
        } else {
            echo "No se pudo actualizar";
        }
        

    }
    public function Eliminar($id_com){
        if ($this->Comunicado->borrarComunicado($id_com)) {

        } else {

        }
        redirect("/Comunicados/index");
        
}  
}





?>