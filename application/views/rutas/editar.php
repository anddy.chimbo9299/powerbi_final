<div class="container-fluid">
    <div class="card">
        <center>
            <h1>
                <b>
                    FORMULARIO EDITAR NUEVA RUTA
                </b>
            </h1>
        </center>
        <div class="card-body">
            <form action="<?php echo site_url('/Rutas/EditarRuta') ?>" method="post" id="frm_nuevo_ruta">
            <div class="col-4">
                        <div class="mb-3">
                         <input type="hidden"  value="<?php echo $rutaEditar->id_rut ?>" type="text" class="form-control" name="id_rut" id="id_rut" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                <div class="row justify-content-center align-items-center g-2">
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="nombre_rut" class="form-label">NOMBRE:</label>
                            <input  value="<?php echo $rutaEditar->nombre_rut?>" type="text" class="form-control" name="nombre_rut" id="nombre_rut" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                    
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="descripcion_rut" class="form-label">DESCRIPCION:</label>
                            <input value="<?php echo $rutaEditar->descripcion_rut?>" type="text" class="form-control" name="descripcion_rut" id="descripcion_rut" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                    
                    
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="estado_rut" class="form-label">ESTADO:</label>
                            <input value="<?php echo $rutaEditar->estado_rut?>" type="text" class="form-control" name="estado_rut" id="estado_rut" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                    </div>
</div>
</div>
                   
                    <center>
                        <button  class="btn btn-primary" type="submit">Guardar</button>
                        <a name="" id="" class="btn btn-dark" href="<?php echo site_url('/Rutas/index')?>" role="button">Cancelar</a>
                    </center>
            </form>
        </div>
    </div>

<script type="text/javascript">
    $("#frm_nuevo_ruta").validate({
        rules: {
            nombre_rut: {
                required: true,
                minlength: 10,
                maxlength: 10,
                digits: true
            },
            descripcion_rut: {
                required: true,

                letras: true
            },
            estado_rut: {
                required: true,

                letras: true
            },
            


        },
        messages: {
            nombre_rut: {
                required: "Nesesariamente debe elegir una",
                minlength: "Cedula incorrecta",
                maxlength: "Cedula incorrecta 10 digitos",
                digits: "Este campo solo acepta numeros",
                number: "Este campo solo acepta numeros",
            },
            descripcion_rut: {
                required: "Porfavor ingrese la descripcion del delito",
                // minlength: "El apellido debe tener minimo 3 digitos ",
                // maxlength: "APellido incorrecto"
            },
            estado_rut: {
                required: "debe ingresar el tiempo de condena para el delito",
                // minlength: "El nombre debe tener 3 dígitos",
                // maxlength: "Nombres incorrectos 50 dígitos",
            },
            


        }
    });
</script>

</div>