<div class="row">
    <div class="col-md-6">
        <h1>Nueva Lectura</h1>
    </div>
    <div>
        <a href="<?php echo site_url();?>/lectura/nuevo">Agregar Nuevo </a>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <form action="<?php echo site_url(); ?>/lecturas/guardar" method="post">

            <div class="row">
                <div  class="col-md-3">
                    <label for=""> Año:</label>
                    <br>
                    <input type="text" class="form-control"name="anio_lec" value="" id="anio_lec" placeholder="Ingrese el año">

                </div>
                <div class="col-md-3">
                    <label for="">Mes:</label>
                    <br>
                    <input type="text" class="form-control"name="mes_lec" value="" id="mes_lec" placeholder="Ingrese el mes">


                    </div>
                <div class="col-md-3">
                    <label for=""> Estado:</label>
                    <br>
                    <input type="text" class="form-control"name="estado_lec" value="" id="estado_lec" placeholder="Ingrese el estado">
                </div>    
                <div class="col-md-3">
                    <label for=""> Lectura Anterior:</label>
                    <br>
                    <input type="text" class="form-control"name="lectura_anterior_lec" value="" id="lectura_anterior_lec" placeholder="Ingrese la lectura anterior">
                </div>   
                
                             
            </div>
            <div class="row">
                <div  class="col-md-3">
                    <label for=""> Lectura Actual:</label>
                    <br>
                    <input type="text" class="form-control" name="lectura_actual_lec" value="" id="lectura_actual_lec" placeholder="Ingrese la lectura actual">
                    
                </div>
                 <div  class="col-md-3">
                    <label for=""> Historia :</label>
                    <br>
                    <select name="fk_id_his" id="fk_id_his" class="form-control">
                        <?php  foreach ($historia as $t) { ?>
                            <option value="<?= $t->id_lec?>"><?= $t->id_lec?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-md-3">
                    <label for="">Consumo:</label>
                    <br>
                    <select name="fk_id_consumo" id="fk_id_consumo" class="form-control">
                        <?php  foreach ($consumo as $t) { ?>
                            <option value="<?= $t->id_lec?>"><?= $t->id_lec?></option>
                        <?php } ?>
                    </select>       
                </div>
                
                                              
            </div>                
            <br>
            <br>
            
            <div class="row">
                <div class="col-md-12 text-center">
                    <button type="submit" name="button" class="btn btn-primary">Guardar</button>&nbsp;
                    <a href="<?php echo site_url(); ?>/lecturas/index" class="btn btn-danger">Cancelar </a>
                </div>
            </div>   
        </form>
    </div>
</div>