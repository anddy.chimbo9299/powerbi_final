<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios extends CI_Controller {

	public function __construct() {
		parent::__construct(); 
		$this->load->model("Usuario");  
	}

	public function index()
	{
		$data["usuarios"]=$this->Usuario->obtenerTodos();
		$this->load->view('header');
		$this->load->view('usuarios/index',$data);
		$this->load->view('footer');
	}

    public function nuevo()
	{
		$this->load->view('header');
		$this->load->view('usuarios/nuevo');
		$this->load->view('footer');
	}

    public function editar()
	{
		$this->load->view('header');
		$this->load->view('usuarios/index');
		$this->load->view('footer');
	}

	public function guardar(){
		$datos = array(
			"nombre_usu"=>$this->input->post("nombre_usu"),
			"apellido_usu"=>$this->input->post("apellido_usu"),
			"email_usu"=> $this->input->post("email_usu"),
			"estado_usu"=>$this->input->post("estado_usu")

		);
		//validamos los campos del formulario 
		if ($this->Usuario->insertar($datos))
		{
			//llamamos la libraria session user_data solo para una vez y flash_data para varias
			
		}else {
			
		}
		redirect ('usuarios/index');

	}

	public function eliminar($id_usu){
	
		  if ($this->Usuario->eliminar($id_usu)) {
			
		  } else {
			
	 
		  }redirect ('usuarios/index');
	}
}
