<?php
date_default_timezone_set('America/Guayaquil');

?>
<br><br>
<div class="contenedor1">
    <h4>
        <center>
            <br><b>
                Nueva Medidor
            </b>

        </center>
    </h4>
    <form class="" id="frm_nuevo_medidor" action="<?php echo site_url("medidors/guardar"); ?>" method="post">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <label for="">Numero: <span medidor</span></label>
                    <br>
                    <input type="text" class="form-control" required name="numero_med" value="" id="numero_med" style="background-color: white;">
                </div>
                <div class="col-md-4">
                    <label for="">Series: <span medidor</span></label>
                    <br>
                    <input type="text" class="form-control" required name="serie_med" value="" id="serie_med" style="background-color: white;">
                </div>
                <div class="col-md-4">
                    <label for="">Marca: <span medidor</span></label>
                    <br>
                    <input type="text" class="form-control" required name="marca_med" value="" id="marca_med" style="background-color: white;">
                </div>
                <div class="col-md-4">
                    <label for="">Observacion: <span medidor</span></label>
                    <br>
                    <input type="text" class="form-control" required name="observacion_med" value="" id="observacion_med" style="background-color: white;">
                </div>
                <div class="col-md-4">
                    <label for="">Estado: <span medidor</span></label>
                    <br>
                    <select class="form-control" required name="estado_med" value="" id="estado_med" style="background-color: white;">
                        <option value="ACTIVO">ACTIVO</option>
                        <option value="INACTIVO">INACTIVO</option>
                    </select>
                </div>
                <div class="col-md-4">
                    <label for="">Fotografia: <span medidor</span></label>
                    <br>
                    <input type="text" class="form-control" required name="foto_med" value="" id="foto_med" style="background-color: white;">
                </div>
                <div class="col-md-4">
                    <label for="">Creacion: <span medidor</span></label>
                    <br>
                    <input type="datetime-local" class="form-control" required name="creacion_med" value="" id="creacion_med" style="background-color: white;">
                </div>
                <div class="col-md-4">
                    <label for="">Actulizar: <span medidor</span></label>
                    <br>
                    <input type="datetime-local" class="form-control" required name="actualizacion_med" value="" id="actualizacion_med" style="background-color: white;">
                </div>
                <div class="col-md-4">
                    <label for="">Lectura: <span medidor</span></label>
                    <br>
                    <input type="number" class="form-control" required name="lectura_inicial_med" value="" id="lectura_inicial_med" style="background-color: white;">
                </div>
                <div class="row">
                    <div class="col-6">
                        <div class="mb-3">
                            <label for="fk_id_tar" class="form-label">Tarifa</label>
                            <select class="form-select form-select" name="fk_id_tar" id="fk_id_tar">
                                <option selected>Selecciona una</option>
                                <?php foreach ($tarifa as $registro) { ?>
                                    <option value="<?php echo $registro->id_tar ?>"><?php echo $registro->nombre_tar ?></option>
                                <?php } ?>
                            </select>
                        </div>

                    </div>
                    <div class="col-6">
                    <div class="mb-3">
                            <label for="fk_id_rut" class="form-label">Tarifa</label>
                            <select class="form-select form-select" name="fk_id_rut" id="fk_id_rut">
                                <option selected>Seleciona una</option>
                                <?php foreach ($ruta as $registro) { ?>
                                    <option value="<?php echo $registro->id_rut ?>"><?php echo $registro->nombre_rut ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <br><br>
        <div class="row">
            <div class="col-md-12 text-center">
                <button type="submit" name="button" class="btn btn-primary">
                    Ingresar
                </button>
                &nbsp;&nbsp;&nbsp;
                <a href="<?php echo site_url(); ?>/medidores/index" class="btn btn-dark">
                    Cancelar
                </a>
            </div>
        </div>
    </form>
</div>