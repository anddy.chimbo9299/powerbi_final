<?php 
class Comunicado extends CI_Model{
    function __construct(){
        parent::__construct();
    }
    public function InsertarCOmunicado($datosCOmunicado){
        return $this->db->insert('comunicado',$datosCOmunicado);
    }
    public function ObtenerComunicados(){
        $datosComunicados = $this->db->get('comunicado');
        if ($datosComunicados->num_rows()>0) {
            return $datosComunicados->result();
        } else {
            return false;
        }
        
    }
    function obtenerComunicado($id_com){
        $this->db->where("id_com",$id_com);
        $comunicado = $this->db->get("comunicado");
        if ($comunicado->num_rows()) {
            return $comunicado->row();

        }
        return false;

    }
    public function actualizarComunicado($id_com,$datos){
        $this->db->where('id_com',$id_com);
        return $this->db->update("comunicado",$datos);
    }
    public function borrarComunicado($id_com){
        $this->db->where('id_com',$id_com);
        return $this->db->delete('comunicado');
        
    }

}

?>