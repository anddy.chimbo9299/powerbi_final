<div class="row">
    <div class="col-md-12">
    <form action="<?php echo site_url('/Perfiles/GuardarPerfil') ?>" method="post" id="frm_nuevo_perfil">
                <div class="row mb-4">
                    <div class="col-md-3">
                        <label for="nombre_imp" class="form-label">NAME:</label>
                        <input type="text" class="form-control" name="nombre_imp" id="nombre_imp" placeholder="Ingrese el nombre">
                    </div>
                    <div class="col-md-4">
                        <div class="mb-3">
                            <label for="descripcion_per" class="form-label">DESCRIPTION:</label>
                            <input type="text" class="form-control" name="descripcion_per" id="descripcion_per" placeholder="Ingrese la descripción">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="mb-3">
                            <label for="estado_per" class="form-label">STATE:</label>
                            <input type="text" class="form-control" name="estado_per" id="estado_per" placeholder="Ingrese el estado">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="mb-3">
                            <label for="creacion_iper" class="form-label">CREATION:</label>
                            <input type="datetime-local" class="form-control" name="creacion_iper" id="creacion_iper" placeholder="Seleccione la fecha de creación">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="mb-3">
                            <label for="actualizacion_per" class="form-label">UPDATE:</label>
                            <input type="datetime-local" class="form-control" name="actualizacion_per" id="actualizacion_per" placeholder="Seleccione la fecha de actualización">
                        </div>
                    </div>
                </div>
                <br>
                <br>

                <div class="row">
                    <div class="col-md-12 text-center">
                        <button class="btn btn-primary" type="submit">Save</button>
                        <a name="" id="" class="btn btn-dark" href="<?php echo site_url('/Perfiles/index') ?>" role="button">Cancel</a>
                    </div>
                </div>
            </form>
    </div>
</div>
<br>
<br>
<br>
<script type="text/javascript">
    $("#frm_nuevo_perfil").validate({
        rules: {
            nombre_per: {
                required: true,
                minlength: 10,
                maxlength: 10,
                digits: true
            },
            descripcion_per: {
                required: true,

                letras: true
            },
            estado_per: {
                required: true,

                letras: true
            },



        },
        messages: {
            nombre_per: {
                required: "Nesesariamente debe elegir una",
                minlength: "Cedula incorrecta",
                maxlength: "Cedula incorrecta 10 digitos",
                digits: "Este campo solo acepta numeros",
                number: "Este campo solo acepta numeros",
            },
            descripcion_per: {
                required: "Porfavor ingrese la descripcion del delito",
                // minlength: "El apellido debe tener minimo 3 digitos ",
                // maxlength: "APellido incorrecto"
            },
            estado_per: {
                required: "debe ingresar el tiempo de condena para el delito",
                // minlength: "El nombre debe tener 3 dígitos",
                // maxlength: "Nombres incorrectos 50 dígitos",
            },



        }
    });// Resto del código de validación...
</script>
