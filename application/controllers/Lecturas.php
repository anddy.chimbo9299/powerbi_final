<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lecturas extends CI_Controller {

	public function __construct() {
		parent::__construct(); 
		$this->load->model("Detalle"); 
        //$this->load->model("Consumo");  
        $this->load->model("Lectura");  
	}

	public function index()
	{
		$data["lectura"]=$this->Lectura->obtenerTodos();
		$this->load->view('header');
		$this->load->view('lecturas/index',$data);
		$this->load->view('footer');
	}

    public function nuevo()
	{
        $data["detalles"]=$this->Detalle->obtenerTodos();
		$this->load->view('header');
		$this->load->view('lecturas/nuevo',$data);
		$this->load->view('footer');
	}

    public function editar($id_lec)
	{
        $data["detalles"]=$this->Detalle->obtenerTodos();
        $data["lecturaEditar"] = $this->Lectura->obtenerPorId($id_lec);
		$this->load->view('header');
		$this->load->view('lecturas/editar',$data);
		$this->load->view('footer');
	}

	public function guardar(){
		$datos = array(
            "anio_lec"=>$this->input->post("anio_lec"),
			"mes_lec"=>$this->input->post("mes_lec"),
            "estado_lec"=>$this->input->post("estado_lec"),
			"lectura_anterior_lec"=>$this->input->post("lectura_anterior_lec"),
            "lectura_actual_lec"=>$this->input->post("lectura_actual_lec"),
            "fk_id_his"=> $this->input->post("fk_id_his"),
            "fk_id_consumo"=>$this->input->post("fk_id_consumo"),

		);
		//validamos los campos del formulario 
		if ($this->Lectura->insertar($datos))
		{
			//llamamos la libraria session user_data solo para una vez y flash_data para varias
			
		}else {
			
		}
		redirect ('lecturas/index');

	}

	public function eliminar($id_lec){
	
		  if ($this->Lectura->eliminar($id_lec)) {
			
		  } else {
			
	 
		  }redirect ('lecturas/index');
	}

    public function procesoActualizar(){
        $datos = array(
            "anio_lec"=>$this->input->post("anio_lec"),
			"mes_lec"=>$this->input->post("mes_lec"),
            "estado_lec"=>$this->input->post("estado_lec"),
			"lectura_anterior_lec"=>$this->input->post("lectura_anterior_lec"),
            "lectura_actual_lec"=>$this->input->post("lectura_actual_lec"),
            "fk_id_his"=> $this->input->post("fk_id_his"),
            "fk_id_consumo"=>$this->input->post("fk_id_consumo"),

		);
        $id_lec=$this->input->post("id_lec");
		//validamos los campos del formulario 
		if ($this->Lectura->actualizar($id_lec,$datos))
		{
			//llamamos la libraria session user_data solo para una vez y flash_data para varias
			
		}else {
			
		}
		redirect ('lecturas/index');

    }
}
