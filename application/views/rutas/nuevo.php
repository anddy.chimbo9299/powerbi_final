<div class="row">
    <div class="col-md-12">
    <h1>  FORMULARIO INGRESO NUEVA RUTA               
            </h1>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
    <form action="<?php echo site_url('/Rutas/GuardarRuta') ?>" method="post" id="frm_nuevo_ruta">
                <div class="row justify-content-center align-items-center g-2">

                    <div class="col-md-4">
                        <div class="mb-3">
                            <label for="nombre_rut" class="form-label">NOMBRE:</label>
                            <input  type="text" class="form-control" name="nombre_rut" id="nombre_rut" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="mb-3">
                            <label for="descripcion_rut" class="form-label">DESCRIPCION:</label>
                            <input  type="text" class="form-control" name="descripcion_rut" id="descripcion_rut" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                   
                    <div class="col-md-4">
                        <div class="mb-3">
                            <label for="estado_rut" class="form-label">ESTADO:</label>
                            <input  type="text" class="form-control" name="estado_rut" id="estado_rut" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>

                   
                <!-- Nueva fila para los botones -->
                <div class="row justify-content-center align-items-center g-2">
                    <div class="col-md-12 text-center">
                        <button class="btn btn-primary" type="submit">Guardar</button>
                        <a name="" id="" class="btn btn-dark" href="<?php echo site_url('/Rutas/index') ?>" role="button">Cancelar</a>
                    </div>
                </div>
            </form>
    </div>
</div>

</div>
