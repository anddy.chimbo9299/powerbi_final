<div class="row">
    <div class="col-md-12">
    <h1> EDITAR CONFIGURACION </h1>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
    <form action="<?php echo site_url('/Configuraciones/EditarConfiguracion') ?>" method="post" id="frm_nuevo_configuracion">
                <div class="col-4">
                        <div class="mb-3">
                            <input type="hidden"  value="<?php echo $configuracionEditar->id_con?>" type="text" class="form-control" name="id_con" id="id_con" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                <div class="row justify-content-center align-items-center g-2">
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="nombre_con" class="form-label">NOMBRE:</label>
                            <input  value="<?php echo $configuracionEditar->nombre_con ?>" type="text" class="form-control" name="nombre_con" id="nombre_con" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                    
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="ruc_con" class="form-label">RUC:</label>
                            <input value="<?php echo $configuracionEditar->ruc_con ?>" type="text" class="form-control" name="ruc_con" id="ruc_con" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="logo_con" class="form-label">LOGO:</label>
                            <input value="<?php echo $configuracionEditar->logo_con ?>" type="text" class="form-control" name="logo_con" id="logo_con" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="telefono_con" class="form-label">TELEFONO:</label>
                            <input value="<?php echo $configuracionEditar->telefono_con ?>" type="text" class="form-control" name="telefono_con" id="telefono_con" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="direccion_con" class="form-label">DIRECCION:</label>
                            <input value="<?php echo $configuracionEditar->direccion_con ?>" type="text" class="form-control" name="direccion_con" id="direccion_con" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="email_con" class="form-label">EMAIL:</label>
                            <input value="<?php echo $configuracionEditar->email_con ?>" type="text" class="form-control" name="email_con" id="email_con" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="servidor_con" class="form-label">SERVIDOR:</label>
                            <input value="<?php echo $configuracionEditar->servidor_con ?>" type="text" class="form-control" name="servidor_con" id="servidor_con" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="puerto_con" class="form-label">PUERTO:</label>
                            <input value="<?php echo $configuracionEditar->puerto_con ?>" type="text" class="form-control" name="puerto_con" id="puerto_con" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="password_con" class="form-label">PASSWORD:</label>
                            <input value="<?php echo $configuracionEditar->password_con ?>" type="text" class="form-control" name="password_con" id="password_con" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="creacion_con" class="form-label">CREACION:</label>
                            <input value="<?php echo $configuracionEditar->creacion_con ?>" type="datetime-local" class="form-control" name="creacion_con" id="creacion_con" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="actualizacion_con" class="form-label">ACTUALIZACION:</label>
                            <input value="<?php echo $configuracionEditar->actualizacion_con ?>" type="datetime-local" class="form-control" name="actualizacion_con" id="actualizacion_con" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="anio_inicial_con" class="form-label">AÑO:</label>
                            <input value="<?php echo $configuracionEditar->anio_inicial_con ?>" type="text" class="form-control" name="anio_inicial_con" id="anio_inicial_con" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="mes_inicial_con" class="form-label">MES:</label>
                            <input value="<?php echo $configuracionEditar->mes_inicial_con ?>" type="text" class="form-control" name="mes_inicial_con" id="mes_inicial_con" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                    <center>
                        <button  class="btn btn-primary" type="submit">Guardar</button>
                        <a name="" id="" class="btn btn-dark" href="<?php echo site_url('/Configuraciones/index') ?>" role="button">Cancelar</a>
                    </center>
            </form>
    </div>
</div>