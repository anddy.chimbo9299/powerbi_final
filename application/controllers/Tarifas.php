<?php 
class Tarifas extends CI_Controller{

    function __construct(){
        parent::__construct();
        $this->load->model('Tarifa');
    }
    public function index(){
        $datos['tarifasIndex']=$this->Tarifa->ObtenerTarifas();
        $this->load->view('header');
        $this->load->view('/tarifas/index',$datos);
        $this->load->view('footer');
    }
    public function nuevo(){
        $this->load->view('header');
        $this->load->view('/tarifas/nuevo');
        $this->load->view('footer');
    }
    public function editar($id_tar){
        $datos['tarifaEditar']=$this->Tarifa->obtenerTarifa($id_tar);
        $this->load->view('header');
        $this->load->view('/tarifas/editar',$datos);
        $this->load->view('footer');
    }
    public function GuardarTarifa(){
        $datosTarifa=array(
			"nombre_tar"=>$this->input->post('nombre_tar'),
			"descripcion_tar"=>$this->input->post('descripcion_tar'),
            "estado_tar"=>$this->input->post('estado_tar'),
			"m3_maximo_tar"=>$this->input->post('m3_maximo_tar'),
      		"tarifa_basica_tar"=>$this->input->post('tarifa_basica_tar'),
      		"tarifa_excedente_tar"=>$this->input->post('tarifa_excedente_tar'),
            "valor_mora_tar"=>$this->input->post('valor_mora_tar')

		);
        if ($this->Tarifa->InsertarTarifa($datosTarifa)) {

        } else {

        }
        redirect("/Tarifas/index");
        
    }
    public function EditarTarifa(){
        $nuevosDatos=array(
            "nombre_tar"=>$this->input->post('nombre_tar'),
			"descripcion_tar"=>$this->input->post('descripcion_tar'),
            "estado_tar"=>$this->input->post('estado_tar'),
			"m3_maximo_tar"=>$this->input->post('m3_maximo_tar'),
      		"tarifa_basica_tar"=>$this->input->post('tarifa_basica_tar'),
      		"tarifa_excedente_tar"=>$this->input->post('tarifa_excedente_tar'),
            "valor_mora_tar"=>$this->input->post('valor_mora_tar')
        );
        // print_r($nuevosDatos);
        $id_tar = $this->input->post('id_tar');
        if ($this->Tarifa->actualizarTarifa($id_tar,$nuevosDatos)) {
            redirect("/Tarifas/index");
        } else {
            echo "No se pudo actualizar";
        }
        

    }
    public function Eliminar($id_tar){
        if ($this->Tarifa->borrarTarifa($id_tar)) {

        } else {

        }
        redirect("/Tarifas/index");
        
}  
}





?>