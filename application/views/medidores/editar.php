<?php
date_default_timezone_set('America/Guayaquil');

?>
<br><br>
<div class="contenedor1">
    <h4>
        <center>
            <br><b>
                Edit Measurer
            </b>

        </center>
    </h4>
    <form class="" id="frm_nuevo_medidor" action="<?php echo site_url("medidors/procesarActualizacion"); ?>" method="post">
        <div class="container">
            <div class="row">
                <input type="hidden" name="id_med" id="id_med" value="<?php echo $medidorEditar->id_med; ?>">
                <div class="col-md-4">
                    <label for="">NUMBER: <span medidor</span></label>
                    <br>
                    <input type="text" class="form-control" required name="numero_med" value="<?php echo $medidorEditar->numero_med; ?>" id="numero_med" style="background-color: white;">
                </div>
                <div class="col-md-4">
                    <label for="">SERIES: <span medidor</span></label>
                    <br>
                    <input type="text" class="form-control" required name="serie_med" value="<?php echo $medidorEditar->serie_med; ?>" id="serie_med" style="background-color: white;">
                </div>
                <div class="col-md-4">
                    <label for="">BRAND: <span medidor</span></label>
                    <br>
                    <input type="text" class="form-control" required name="marca_med" value="<?php echo $medidorEditar->marca_med; ?>" id="marca_med" style="background-color: white;">
                </div>
                <div class="col-md-4">
                    <label for="">OBSERVATION: <span medidor</span></label>
                    <br>
                    <input type="text" class="form-control" required name="observacion_med" value="<?php echo $medidorEditar->observacion_med; ?>" id="observacion_med" style="background-color: white;">
                </div>
                <div class="col-md-4">
                    <label for="">STATE: <span medidor</span></label>
                    <br>
                    <select class="form-control" required name="estado_med" value="<?php echo $medidorEditar->estado_med; ?>" id="estado_med" style="background-color: white;">
                        <option value="ACTIVO">ACTIVO</option>
                        <option value="INACTIVO">INACTIVO</option>
                    </select>
                </div>
                <div class="col-md-4">
                    <label for="">PHOTO: <span medidor</span></label>
                    <br>
                    <input type="text" class="form-control" required name="foto_med" value="<?php echo $medidorEditar->foto_med; ?>" id="foto_med" style="background-color: white;">
                </div>
                <div class="col-md-4">
                    <label for="">CREATION: <span medidor</span></label>
                    <br>
                    <input type="datetime-local" class="form-control" required name="creacion_med" value="<?php echo $medidorEditar->creacion_med; ?>" id="creacion_med" style="background-color: white;">
                </div>
                <div class="col-md-4">
                    <label for="">UPDATE: <span medidor</span></label>
                    <br>
                    <input type="datetime-local" class="form-control" required name="actualizacion_med" value="<?php echo $medidorEditar->actualizacion_med; ?>" id="actualizacion_med" style="background-color: white;">
                </div>
                <div class="col-md-4">
                    <label for="">READING: <span medidor</span></label>
                    <br>
                    <input type="number" class="form-control" required name="lectura_inicial_med" value="<?php echo $medidorEditar->lectura_inicial_med; ?>" id="lectura_inicial_med" style="background-color: white;">
                </div>

            </div>
            <div class="row">
                    <div class="col-6">
                        <div class="mb-3">
                            <label for="fk_id_tar" class="form-label">Tarifa</label>
                            <select class="form-select form-select" name="fk_id_tar" id="fk_id_tar">
                                <option value="<?php echo $medidorEditar->fk_id_tar ?>" selected><?php echo $medidorEditar->fk_id_tar ?></option>
                                <?php foreach ($tarifa as $registro) { ?>
                                    <option value="<?php echo $registro->id_tar ?>"><?php echo $registro->nombre_tar ?></option>
                                <?php } ?>
                            </select>
                        </div>

                    </div>
                    <div class="col-6">
                    <div class="mb-3">
                            <label for="fk_id_rut" class="form-label">Tarifa</label>
                            <select class="form-select form-select" name="fk_id_rut" id="fk_id_rut">
                                <option value="<?php echo $medidorEditar->fk_id_rut ?> " selected><?php echo $medidorEditar->fk_id_rut ?> </option>
                                <?php foreach ($ruta as $registro) { ?>
                                    <option value="<?php echo $registro->id_rut ?>"><?php echo $registro->nombre_rut ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center">
                <button type="submit" name="button" class="btn btn-primary">
                    keep
                </button>
                &nbsp;&nbsp;&nbsp;
                <a href="<?php echo site_url(); ?>/medidors/index" class="btn btn-dark">
                    Cancel
                </a>
            </div>
        </div>
    </form>
</div>