<div class="row">
    <div class="col-md-12">
    <center>
        <br>
           <div class="row">
           <h1>Listado de Recaudaciones</h1>
            <div class="col-4">
                <br>
                
                 <a name="" id="" class="btn btn-primary" href="<?php echo site_url('/recaudaciones/nuevo') ?>" role="button">Nuevo Recaudacion</a>
                </div>
            
           </div>
        </center>
    </div>
</div>


<br>
<br>
<div class="row">
    <div class="col-md-12">
        <?php if ($recaudaciones): ?>
            <table class="table  table-striped" id="tablaRecaudacion">
                <thead>
                    <th>ID</th>
                    <th>Número de Factura</th>
                    <th>Numero de Autorizacion</th>
                    <th>Ambiente </th>
                    <th>Emison</th>
                    <th>Correo</th>                                                           
                    <th>Fecha de Emision</th>                  
                    <th>Estado</th>
                    <th>Acciones</th>
                </thead>
                
                <tbody>
                    <?php foreach ($recaudaciones as $filaTemporal): ?>
                        <tr>
                            <td><?php echo $filaTemporal->id_rec ?></td>
                            <td><?php echo $filaTemporal->numero_factura_rec ?></td>
                            <td><?php echo $filaTemporal->numero_autorizacion_rec ?></td>
                            <td><?php echo $filaTemporal->ambiente_rec ?> </td>
                            <td> <?php echo $filaTemporal->emision_rev ?></td>
                            <td><?php echo $filaTemporal->email_rec ?></td>                            
                            <th><?php echo $filaTemporal->fecha_emision_rec ?></th>                            
                            <th><?php echo $filaTemporal->estado_rec ?></th>
                            <td class="text-center" >
                                <a href="<?php echo site_url(); ?>/recaudaciones/editar/<?php echo $filaTemporal->id_rec; ?>" title="Editar Recaudacion" >
                                    <i class="mdi  mdi-pencil">Editar</i>
                                </a>
                                &nbsp;&nbsp;

                                <a href="<?php echo site_url(); ?>/recaudaciones/eliminar/<?php echo $filaTemporal->id_rec; ?>" title="Borrar Recaudacion" style="color:red" onclick="return confirm('Esta seguro de borra el registro?');">
                                    <i class="mdi  mdi-close">Eliminar</i>
                                </a>
            
                            </td>
                        </tr>         

                    <?php endforeach; ?>
                    </tbody>
                </table>

                <?php else: ?>
                <h1>No hay datos</h1>

        <?php endif; ?>

       
    </div>
</div>

<script type="text/javascript">
    $("#tablaRecaudacion")
    .DataTable();
</script>