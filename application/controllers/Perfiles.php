<?php 
class Perfiles extends CI_Controller{

    function __construct(){
        parent::__construct();
        $this->load->model('Perfil');
    }
    public function index(){
        $datos['perfilesIndex']=$this->Perfil->ObtenerPerfiles();
        $this->load->view('header');
        $this->load->view('/perfiles/index',$datos);
        $this->load->view('footer');
    }
    public function nuevo(){
        $this->load->view('header');
        $this->load->view('/perfiles/nuevo');
        $this->load->view('footer');
    }
    public function editar($id_per){
        $datos['perfilEditar']=$this->Perfil->obtenerPerfil($id_per);
        $this->load->view('header');
        $this->load->view('/perfiles/editar',$datos);
        $this->load->view('footer');
    }
    public function GuardarPerfil(){
        $datosPerfil=array(
			"nombre_per"=>$this->input->post('nombre_per'),
			"descripcion_per"=>$this->input->post('descripcion_per'),
      		"estado_per"=>$this->input->post('estado_per'),
      		"creacion_per"=>$this->input->post('creacion_per'),
      		"actualizacion_per"=>$this->input->post('actualizacion_per')
		);
        if ($this->Perfil->InsertarPerfil($datosPerfil)) {

        } else {

        }
        redirect("/Perfiles/index");
        
    }
    public function EditarPerfil(){
        $nuevosDatos=array(
            "nombre_per"=>$this->input->post('nombre_per'),
			"descripcion_per"=>$this->input->post('descripcion_per'),
      		"estado_per"=>$this->input->post('estado_per'),
      		"creacion_per"=>$this->input->post('creacion_per'),
      		"actualizacion_per"=>$this->input->post('actualizacion_per')
        );
        // print_r($nuevosDatos);
        $id_per = $this->input->post('id_per');
        if ($this->Perfil->actualizarPerfil($id_per,$nuevosDatos)) {
            redirect("/Perfiles/index");
        } else {
            echo "No se pudo actualizar";
        }
        

    }
    public function Eliminar($id_per){
        if ($this->Perfil->borrarPerfil($id_per)) {

        } else {

        }
        redirect("/Perfiles/index");
        
}  
}





?>