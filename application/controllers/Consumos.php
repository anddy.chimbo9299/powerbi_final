<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Consumos extends CI_Controller {

	public function __construct() {
		parent::__construct(); 
		
        $this->load->model("Consumo");  
          
	}

	public function index()
	{
		$data["consumo"]=$this->Consumo->obtenerTodos();
		$this->load->view('header');
		$this->load->view('consumos/index',$data);
		$this->load->view('footer');
	}

    public function nuevo()
	{
        
		$this->load->view('header');
		$this->load->view('consumos/nuevo');
		$this->load->view('footer');
	}

    public function editar($id_consumo)
	{
        
        $data["consumoEditar"] = $this->Consumo->obtenerPorId($id_consumo);
		$this->load->view('header');
		$this->load->view('consumos/editar',$data);
		$this->load->view('footer');
	}

	public function guardar(){
		$datos = array(
            "anio_consumo"=>$this->input->post("anio_consumo"),
			"mes_consumo"=>$this->input->post("mes_consumo"),
            "estado_consumo"=>$this->input->post("estado_consumo"),
			"numero_mes_consumo"=>$this->input->post("numero_mes_consumo"),
            "fecha_vencimiento_consumo"=>$this->input->post("fecha_vencimiento_consumo"),
        
		);
		//validamos los campos del formulario 
		if ($this->Consumo->insertar($datos))
		{
			
		}else {
			
		}
		redirect ('consumos/index');

	}

	public function eliminar($id_consumo){
	
		  if ($this->Consumo->eliminar($id_consumo)) {
			
		  } else {
			
	 
		  }redirect ('consumos/index');
	}

    public function procesoActualizar(){
        $datos = array(
            "anio_consumo"=>$this->input->post("anio_consumo"),
			"mes_consumo"=>$this->input->post("mes_consumo"),
            "estado_consumo"=>$this->input->post("estado_consumo"),
			"numero_mes_consumo"=>$this->input->post("numero_mes_consumo"),
            "fecha_vencimiento_consumo"=>$this->input->post("fecha_vencimiento_consumo"),

		);
        $id_consumo=$this->input->post("id_consumo");
		//validamos los campos del formulario 
		if ($this->Consumo->actualizar($id_consumo,$datos))
		{
			//llamamos la libraria session user_data solo para una vez y flash_data para varias
			
		}else {
			
		}
		redirect ('consumos/index');

    }
}
