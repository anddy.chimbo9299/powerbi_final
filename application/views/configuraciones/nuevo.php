<div class="container-fluid">
    <div class="card">
        <center>
            <h1>
                <b>
                    FORMULARIO NUEVAS CONFIGURACIONES
                </b>
            </h1>
        </center>
        <div class="card-body">
            <form action="<?php echo site_url('/Configuraciones/GuardarConfiguracion') ?>" method="post" id="frm_nuevo_configuracion">
                <div class="row justify-content-center align-items-center g-2">

                    <div class="col-4">
                    <div class="mb-3">
                            <input type="hidden" type="text" class="form-control" name="id_con" id="id_con" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                <div class="row justify-content-center align-items-center g-2">
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="nombre_con" class="form-label">NOMBRE:</label>
                            <input type="text" class="form-control" name="nombre_con" id="nombre_con" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                    
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="ruc_con" class="form-label">RUC:</label>
                            <input type="text" class="form-control" name="ruc_con" id="ruc_con" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="logo_con" class="form-label">LOGO:</label>
                            <input  type="text" class="form-control" name="logo_con" id="logo_con" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="telefono_con" class="form-label">TELEFONO:</label>
                            <input type="text" class="form-control" name="telefono_con" id="telefono_con" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="direccion_con" class="form-label">DIRECCION:</label>
                            <input type="text" class="form-control" name="direccion_con" id="direccion_con" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="email_con" class="form-label">EMAIL:</label>
                            <input  type="text" class="form-control" name="email_con" id="email_con" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="servidor_con" class="form-label">SERVIDOR:</label>
                            <input type="text" class="form-control" name="servidor_con" id="servidor_con" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="puerto_con" class="form-label">PUERTO:</label>
                            <input  type="text" class="form-control" name="puerto_con" id="puerto_con" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="password_con" class="form-label">PASSWORD:</label>
                            <input  type="text" class="form-control" name="password_con" id="password_con" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="creacion_con" class="form-label">CREACION:</label>
                            <input  type="datetime-local" class="form-control" name="creacion_con" id="creacion_con" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="actualizacion_con" class="form-label">ACTUALIZACION:</label>
                            <input  type="datetime-local" class="form-control" name="actualizacion_con" id="actualizacion_con" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="anio_inicial_con" class="form-label">AÑO:</label>
                            <input  type="text" class="form-control" name="anio_inicial_con" id="anio_inicial_con" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                    
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="mes_inicial_con" class="form-label">MES:</label>
                            <input  type="text" class="form-control" name="mes_inicial_con" id="mes_inicial_con" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                    </div>
                    <div class="row">
                    <div class="col-md-12 text-center">
                        <button class="btn btn-primary" type="submit">Guardar</button>
                        <a name="" id="" class="btn btn-dark" href="<?php echo site_url('/Configuraciones/index') ?>" role="button">Cancelar</a>
                    </div>
                </div>
            </form>
        </div>
        </div>
    
        </div>
        </div>
    

<script type="text/javascript">
      $("#frm_nuevo_configuracion").validate({
        rules: {
            nombre_con: {
                required: true,
                minlength: 10,
                maxlength: 10,
                digits: true
            },
            ruc_con: {
                required: true,

                letras: true
            },
            logo_con: {
                required: true,

                letras: true
            },
            


        },
        messages: {
            nombre_con: {
                required: "Nesesariamente debe elegir una",
                minlength: "Cedula incorrecta",
                maxlength: "Cedula incorrecta 10 digitos",
                digits: "Este campo solo acepta numeros",
                number: "Este campo solo acepta numeros",
            },
            ruc_con: {
                required: "Porfavor ingrese la descripcion del delito",
                // minlength: "El apellido debe tener minimo 3 digitos ",
                // maxlength: "APellido incorrecto"
            },
            logo_con: {
                required: "debe ingresar el tiempo de condena para el delito",
                // minlength: "El nombre debe tener 3 dígitos",
                // maxlength: "Nombres incorrectos 50 dígitos",
            },
            


        }
    });
</script>

</div>
