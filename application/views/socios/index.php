<div class="row">
    <div class="col-md-12">
    <center>
        <br>
           <div class="row">
                <h1>Listado de Socios</h1>
            <div class="col-4">
                <br>
                
                 <a name="" id="" class="btn btn-primary" href="<?php echo site_url('/socios/nuevo') ?>" role="button">Nuevo Socios</a>
                </div>
            
           </div>
        </center>
    </div>
</div>

<br>
<br>
<div class="row">
    <div class="col-md-12">
        <?php if ($socios): ?>
            <table class="table  table-striped" id="tablaSocios">
                <thead>
                    <th>ID</th>
                    <th>Nombre Socio</th>
                    <th>Apellidos Socios</th>
                    <th>Correo Electrónico </th>
                    <th>Identificacion</th>
                    <th>Fechas Nacimiento</th>
                    <th>Tipo Socio</th>
                    <th>Acciones</th>
                </thead>
                
                <tbody>
                    <?php foreach ($socios as $filaTemporal): ?>
                        <tr>
                            <td><?php echo $filaTemporal->id_soc ?></td>
                            <td><?php echo $filaTemporal->nombres_soc ?></td>
                            <td><?php echo $filaTemporal->primer_apellido_soc ?> <?php echo $filaTemporal->segundo_apellido_soc ?> </td>
                            <td> <?php echo $filaTemporal->email_soc ?></td>
                            <td><?php echo $filaTemporal->identificacion_soc ?></td>
                            <th><?php echo $filaTemporal->fecha_nacimiento_soc ?></th>
                            <th><?php echo $filaTemporal->tipo_soc ?></th>
                            
                            <td class="text-center" >
                                <a href="<?php echo site_url(); ?>/socios/editar/<?php echo $filaTemporal->id_soc; ?>" title="Editar Socio" >
                                    <i class="mdi  mdi-pencil">Editar</i>
                                </a>
                                &nbsp;&nbsp;

                                <a href="<?php echo site_url(); ?>/socios/eliminar/<?php echo $filaTemporal->id_soc; ?>" title="Borrar Socio" style="color:red" onclick="return confirm('Esta seguro de borra el registro?');">
                                    <i class="mdi  mdi-close">Eliminar</i>
                                </a>
            
                            </td>
                        </tr>         

                    <?php endforeach; ?>
                    </tbody>
                </table>

                <?php else: ?>
                <h1>No hay datos</h1>

        <?php endif; ?>

       
    </div>
</div>

<script type="text/javascript">
    $("#tablaSocios")
    .DataTable();
</script>