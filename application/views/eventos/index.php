<h1>Listado de Eventos</h1>
<br>
<br>
<div class="row">
    <div class="col-md-12">
        <?php if ($evento): ?>
            <table class="table  table-striped" id="tablaDetalle">
                <thead>
                    <th>ID</th>
                    <th>Descripcion</th>
                    <th>Fecha</th>
                    <th>Lugar </th>
                    
                    <th>tipo_evento</th>
                    <th>Acciones</th>
                </thead>

                <tbody>
                    <?php foreach ($evento as $filaTemporal): ?>
                        <tr>
                            <td><?php echo $filaTemporal->id_eve ?></td>
                            <td><?php echo $filaTemporal->descripcion_eve?></td>
                            <td><?php echo $filaTemporal->fecha_hora_eve ?>  </td>
                            <td> <?php echo $filaTemporal->lugar_eve ?></td>
                            <th><?php echo $filaTemporal->fk_id_te ?></th>

                            <td class="text-center" >
                                <a href="<?php echo site_url(); ?>/eventos/editar/<?php echo $filaTemporal->id_eve; ?>" title="Editar evento" >
                                    <i class="mdi  mdi-pencil">Editar</i>
                                </a>
                                &nbsp;&nbsp;

                                <a href="<?php echo site_url(); ?>/eventos/eliminar/<?php echo $filaTemporal->id_eve; ?>" title="Borrar evento" style="color:red" onclick="return confirm('Esta seguro de borra el registro?');">
                                    <i class="mdi  mdi-close">Eliminar</i>
                                </a>

                            </td>
                        </tr>

                    <?php endforeach; ?>
                    </tbody>
                </table>

                <?php else: ?>
                <h1>No hay datos</h1>

        <?php endif; ?>


    </div>
</div>

<script type="text/javascript">
    $("#tablaDetalle")
    .DataTable();
</script>
