<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Detalles extends CI_Controller {

	public function __construct() {
		parent::__construct(); 
		$this->load->model("Detalle"); 
        $this->load->model("Recaudacion");  
		$this->load->model("Lectura");  
	}

	public function index()
	{
		$data["detalle"]=$this->Detalle->obtenerTodos();
		$this->load->view('header');
		$this->load->view('detalles/index',$data);
		$this->load->view('footer');
	}

    public function nuevo()
	{
        $data["recaudacion"]=$this->Recaudacion->obtenerTodos();
		$data["lectura"]=$this->Lectura->obtenerTodos();
		$this->load->view('header');
		$this->load->view('detalles/nuevo',$data);
		$this->load->view('footer');
	}

    public function editar($id_det)
	{
        $data["recaudacion"]=$this->Recaudacion->obtenerTodos();
        $data["detalleEditar"] = $this->Detalle->obtenerPorId($id_det);
		$this->load->view('header');
		$this->load->view('detalles/editar',$data);
		$this->load->view('footer');
	}

	public function guardar(){
		$datos = array(
            "fk_id_lec"=>$this->input->post("fk_id_lec"),
			"fk_id_rec"=>$this->input->post("fk_id_rec"),
            "cantidad_det"=>$this->input->post("cantidad_det"),
			"detalle_det"=>$this->input->post("detalle_det"),
            "valor_unitario_det"=>$this->input->post("valor_unitario_det"),
            "subtotal_det"=> $this->input->post("subtotal_det"),
            "iva_det"=>$this->input->post("iva_det"),

		);
		//validamos los campos del formulario 
		if ($this->Detalle->insertar($datos))
		{
			//llamamos la libraria session user_data solo para una vez y flash_data para varias
			
		}else {
			
		}
		redirect ('detalles/index');

	}

	public function eliminar($id_det){
	
		  if ($this->Detalle->eliminar($id_det)) {
			
		  } else {
			
	 
		  }redirect ('detalles/index');
	}

    public function procesoActualizar(){
        $datos = array(
            "fk_id_lec"=>$this->input->post("fk_id_lec"),
			"fk_id_rec"=>$this->input->post("fk_id_rec"),
            "cantidad_det"=>$this->input->post("cantidad_det"),
			"detalle_det"=>$this->input->post("detalle_det"),
            "valor_unitario_det"=>$this->input->post("valor_unitario_det"),
            "subtotal_det"=> $this->input->post("subtotal_det"),
            "iva_det"=>$this->input->post("iva_det"),

		);
        $id_det=$this->input->post("id_det");
		//validamos los campos del formulario 
		if ($this->Detalle->actualizar($id_det,$datos))
		{
			//llamamos la libraria session user_data solo para una vez y flash_data para varias
			
		}else {
			
		}
		redirect ('detalles/index');

    }
}
