<h1>Nuevo Detalle</h1>
<div class="row">
    <div class="col-md-12">
        <form action="<?php echo site_url(); ?>/detalles/procesoActualizar" method="post">
            <input type="text" id="id_det" name="id_det" value="<?php echo $detalleEditar->id_det?>" hidden>
            <div class="row">

                <div class="col-md-3">
                    <label for=""> Descripcion:</label>
                    <br>
                    <input type="text" class="form-control"name="descripcion_eve" value="<?php echo $eventoEditar->descripcion_eve?>" id="descripcion_eve" placeholder="Ingrese la cantidad">
                </div>

            </div>
            <div class="row">
                <div  class="col-md-3">
                    <label for=""> Fecha:</label>
                    <br>
                    <input type="datetime" class="form-control" name="fecha_hora_eve" value="<?php echo $eventoEditar->fecha_hora_eve?>" id="fecha_hora_eve" placeholder="Ingrese la fecha">
                </div>
                 <div  class="col-md-3">
                    <label for=""> Lugar:</label>
                    <br>
                    <input type="text" class="form-control" name="lugar_eve" value="<?php echo $eventoEditar->lugar_eve?>" id="lugar_eve" placeholder="Ingrese el lugar">
                </div>

                <div  class="col-md-3">
                    <label for=""> Tipo evento:</label>
                    <br>
                    <select name="fk_id_te" id="fk_id_te" class="form-control">
                        <?php  foreach ($usuario as $t) { ?>
                            <option value="<?= $t->id_usu?>"><?= $t->nombre_usu?></option>
                        <?php } ?>
                    </select>
                </div>

            </div>
            <br>
            <br>

            <div class="row">
                <div class="col-md-12 text-center">
                    <button type="submit" name="button" class="btn btn-primary">Guardar</button>&nbsp;
                    <a href="<?php echo site_url(); ?>/eventos/index" class="btn btn-danger">Cancelar </a>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
     $("#fk_id_te").val('<?php echo $eventoEditar->fk_id_te; ?>');
</script>
