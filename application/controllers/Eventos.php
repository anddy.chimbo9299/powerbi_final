<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Eventos extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model("Evento");
        

	}

	public function index()
	{
		$data["evento"]=$this->Evento->obtenerTodos();
		$this->load->view('header');
		$this->load->view('eventos/index',$data);
		$this->load->view('footer');
	}

    public function nuevo()
	{
		$data["evento"]=$this->Evento->obtenerTodos();
		$this->load->view('header');
		$this->load->view('eventos/nuevo',$data);
		$this->load->view('footer');
	}

    public function editar($id_eve)
	{
        $data["evento"]=$this->Evento->obtenerTodos();
		$this->load->view('header');
		$this->load->view('eventos/editar',$data);
		$this->load->view('footer');
	}

	public function guardar(){
		$datos = array(

            "descripcion_eve"=>$this->input->post("descripcion_eve"),
			"fecha_hora_eve"=>$this->input->post("fecha_hora_eve"),
            "lugar_eve"=>$this->input->post("lugar_eve"),
            "fk_id_rec"=>$this->input->post("fk_id_rec"),

		);
		//validamos los campos del formulario
		if ($this->Evento->insertar($datos))
		{
			//llamamos la libraria session user_data solo para una vez y flash_data para varias

		}else {

		}
		redirect ('eventos/index');

	}

	public function eliminar($id_eve){

		  if ($this->Detalle->eliminar($id_eve)) {

		  } else {


		  }redirect ('eventos/index');
	}

    public function procesoActualizar(){
        $datos = array(
          "descripcion_eve"=>$this->input->post("descripcion_eve"),
    "fecha_hora_eve"=>$this->input->post("fecha_hora_eve"),
          "lugar_eve"=>$this->input->post("lugar_eve"),
          "fk_id_rec"=>$this->input->post("fk_id_rec"),

		);
        $id_eve=$this->input->post("id_eve");
		//validamos los campos del formulario
		if ($this->Evento->actualizar($id_eve,$datos))
		{
			//llamamos la libraria session user_data solo para una vez y flash_data para varias

		}else {

		}
		redirect ('eventos/index');

    }
}
