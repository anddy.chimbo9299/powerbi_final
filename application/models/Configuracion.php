<?php 
class Configuracion extends CI_Model{
    function __construct(){
        parent::__construct();
    }
    public function InsertarConfiguracion($datosConfiguracion){
        return $this->db->insert('configuracion',$datosConfiguracion);
    }
    public function ObtenerConfiguraciones(){
        $datosConfiguracion = $this->db->get('configuracion');
        if ($datosConfiguracion->num_rows()>0) {
            return $datosConfiguracion->result();
        } else {
            return false;
        }
        
    }
    function obtenerConfiguracion($id_con){
        $this->db->where("id_con",$id_con);
        $configuracion = $this->db->get("configuracion");
        if ($configuracion->num_rows()) {
            return $configuracion->row();

        }
        return false;

    }
    public function actualizarConfiguracion($id_con,$datos){
        $this->db->where('id_con',$id_con);
        return $this->db->update("configuracion",$datos);
    }
    public function borrarConfiguracion($id_con){
        $this->db->where('id_con',$id_con);
        return $this->db->delete('configuracion');
        
    }

}

?>