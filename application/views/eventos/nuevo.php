<h1>Nuevo Evento</h1>
<div class="row">
    <div class="col-md-12">
        <form action="<?php echo site_url(); ?>/eventos/guardar" method="post">

            <div class="row">

                <div class="col-md-3">
                    <label for=""> Descripcion:</label>
                    <br>
                    <input type="text" class="form-control"name="descripcion_eve" value="" id="descripcion_eve" placeholder="Ingrese la descripcion">
                </div>

            </div>
            <div class="row">
                <div  class="col-md-3">
                    <label for=""> Fecha:</label>
                    <br>
                    <input type="datetime" class="form-control" name="fecha_hora_eve" value="" id="fecha_hora_eve" placeholder="Ingrese la fecha">
                </div>
                 <div  class="col-md-3">
                    <label for=""> Lugar evento :</label>
                    <br>
                    <input type="text" class="form-control" name="lugar_eve" value="" id="lugar_eve" placeholder="Ingrese el lugar del evento">
                </div>


                <div  class="col-md-3">
                    <label for=""> Tipo evento:</label>
                    <br>
                    <select name="fk_id_te" id="fk_id_te" class="form-control">
                        <?php  foreach ($evento as $t) { ?>
                            <option value="<?= $t->fk_id_te?>"><?= $t->descripcion_eve?></option>
                        <?php } ?>
                    </select>
                </div>


            </div>
            <br>
            <br>

            <div class="row">
                <div class="col-md-12 text-center">
                    <button type="submit" name="button" class="btn btn-primary">Guardar</button>&nbsp;
                    <a href="<?php echo site_url(); ?>/eventos/index" class="btn btn-danger">Cancelar </a>
                </div>
            </div>
        </form>
    </div>
</div>
