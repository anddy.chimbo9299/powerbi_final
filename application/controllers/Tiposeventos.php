<?php 
class TiposEventos extends CI_Controller{

    function __construct(){
        parent::__construct();
        $this->load->model('Tipoevento');
    }
    public function index(){
        $datos['tiposeventosIndex']=$this->Tipoevento->ObtenerTiposeventos();
        $this->load->view('header');
        $this->load->view('/tiposeventos/index',$datos);
        $this->load->view('footer');
    }
    public function nuevo(){
        $this->load->view('header');
        $this->load->view('/tiposeventos/nuevo');
        $this->load->view('footer');
    }
    public function editar($id_te){
        $datos['tipoeventoEditar']=$this->Tipoevento->obtenerTipoevento($id_te);
        $this->load->view('header');
        $this->load->view('/tiposeventos/editar',$datos);
        $this->load->view('footer');
    }
    public function GuardarTipoevento(){
        $datosTipoevento=array(
			"nombre_te"=>$this->input->post('nombre_te'),
      		"estado_te"=>$this->input->post('estado_te'),
      		"creacion_te"=>$this->input->post('creacion_te'),
      		"actualizacion_te"=>$this->input->post('actualizacion_te')
		);
        if ($this->Tipoevento->InsertarTipoevento($datosTipoevento)) {

        } else {

        }
        redirect("/Tiposeventos/index");
        
    }
    public function EditarTipoevento(){
        $nuevosDatos=array(
            "nombre_te"=>$this->input->post('nombre_te'),
            "estado_te"=>$this->input->post('estado_te'),
            "creacion_te"=>$this->input->post('creacion_te'),
            "actualizacion_te"=>$this->input->post('actualizacion_te')
        );
        // print_r($nuevosDatos);
        $id_te = $this->input->post('id_te');
        if ($this->Tipoevento->actualizarTipoevento($id_te,$nuevosDatos)) {
            redirect("/Tiposeventos/index");
        } else {
            echo "No se pudo actualizar";
        }
        

    }
    public function Eliminar($id_te){
        if ($this->Tipoevento->borrarTipoevento($id_te)) {

        } else {

        }
        redirect("/Tiposeventos/index");
        
}  
}





?>