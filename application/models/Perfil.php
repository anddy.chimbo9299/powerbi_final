<?php 
class Perfil extends CI_Model{
    function __construct(){
        parent::__construct();
    }
    public function InsertarPerfil($datosPerfil){
        return $this->db->insert('perfil',$datosPerfil);
    }
    public function ObtenerPerfiles(){
        $datosPerfiles = $this->db->get('perfil');
        if ($datosPerfiles->num_rows()>0) {
            return $datosPerfiles->result();
        } else {
            return false;
        }
        
    }
    function obtenerPerfil($id_per){
        $this->db->where("id_per",$id_per);
        $perfil = $this->db->get("perfil");
        if ($perfil->num_rows()) {
            return $perfil->row();

        }
        return false;

    }
    public function actualizarPerfil($id_per,$datos){
        $this->db->where('id_per',$id_per);
        return $this->db->update("perfil",$datos);
    }
    public function borrarPerfil($id_per){
        $this->db->where('id_per',$id_per);
        return $this->db->delete('perfil');
        
    }

}

?>